<?php

namespace Social\Core;

/**
 * Class View
 * Formateo de datos para su devolución en formato JSON
 */
class View
{
    /**
     * Renderiza respuesta (o mensaje de error de haberlo) en formato JSON y la devuelve.
     *
     * @param array $data
     * @param String $error
     * @return void
     */
    public static function responseToJSON($data, $error)
    {
        header('Content-Type: application/json; charset=utf-8');
        $response = $data;
        
        if (is_null($response)) {
            $response = ["status" => "error", "data" => $error];
        } else {
            $response = ["status" => "ok", "data" => $response];
        }
        echo json_encode($response);
    }

    

}
