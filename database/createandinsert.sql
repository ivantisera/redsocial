DROP DATABASE IF EXISTS SOCIAL_tisera_dw4atn;
CREATE DATABASE SOCIAL_tisera_dw4atn;
USE SOCIAL_tisera_dw4atn;

CREATE TABLE USERS(
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(50) NOT NULL,
  email varchar(100) NOT NULL UNIQUE,
  password VARCHAR(64) NOT NULL,
  foto LONGTEXT,
  descripcion TEXT,
  residencia VARCHAR(200)
)ENGINE=innoDB;

CREATE TABLE TIPOPOSTS(
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    tipo varchar(50)
)ENGINE=innoDB;

CREATE TABLE POSTS(
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  autor_id INTEGER NOT NULL,
  tipo_id INTEGER NOT NULL,
  contenido TEXT,
  FECHA TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (autor_id) REFERENCES USERS(id),
  FOREIGN KEY (tipo_id) REFERENCES TIPOPOSTS(id)
)ENGINE=innoDB;

CREATE TABLE IMAGENES(
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    imagen VARCHAR(255) NOT NULL,
    post_id INTEGER NOT NULL,
    FOREIGN KEY (post_id) REFERENCES POSTS(id)
)ENGINE=innodb;


CREATE TABLE COMENTARIOS(
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  autor_id INTEGER NOT NULL,
  post_id INTEGER NOT NULL,
  comentario text NOT NULL,
  fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (autor_id) REFERENCES USERS(id),
  FOREIGN KEY (post_id) REFERENCES POSTS(id)
)ENGINE=innoDB;


CREATE TABLE AMISTADES(
  requester_id INTEGER NOT NULL,
  requested_id INTEGER NOT NULL,
  estado VARCHAR(20) NOT NULL,
  FOREIGN KEY (requester_id) REFERENCES USERS(id),
  FOREIGN KEY (requested_id) REFERENCES USERS(id),
  PRIMARY KEY (requester_id, requested_id)
)ENGINE=innoDB;

CREATE TABLE FAVORITOS(
  post_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  has_modifications BOOLEAN NOT NULL,
  FOREIGN KEY (post_id) REFERENCES POSTS(id),
  FOREIGN KEY (user_id) REFERENCES USERS(id),
  PRIMARY KEY (post_id, user_id)
)ENGINE=innoDB;


INSERT INTO USERS (username, password, email, foto, descripcion, residencia)  VALUES ("Sofía Reyes", "$2y$10$eVpFabQtP8RtzuTq2FInuOjdiD5IUj6yZY8iNINXo7TsRPbohYkQ6", "sofia@mail.com", "", "Soy estudiante de derecho. Me gustan los gatitos.","Palermo, CABA, Argentina");
INSERT INTO USERS (username, password, email, foto, descripcion, residencia)  VALUES ("Pabla Musica", "$2y$10$eVpFabQtP8RtzuTq2FInuOjdiD5IUj6yZY8iNINXo7TsRPbohYkQ6", "lucas@mail.com",  "","Me gusta el arte. Todo tipo de arte", "Ramos Mejía, Bs. As., Argentina");
INSERT INTO USERS (username, password, email, foto, descripcion, residencia)  VALUES ("Aye Martin", "$2y$10$eVpFabQtP8RtzuTq2FInuOjdiD5IUj6yZY8iNINXo7TsRPbohYkQ6", "valeria@mail.com", "", "Escribo, leo y edito literatura LGBTIQ", "Rosario, Santa Fe, Argentina");
INSERT INTO USERS (username, password, email, foto, descripcion, residencia)  VALUES ("Flor Borrachia", "$2y$10$eVpFabQtP8RtzuTq2FInuOjdiD5IUj6yZY8iNINXo7TsRPbohYkQ6", "flor@mail.com", "", "Me gustan los gallegos", "Rosario, Santa Fe, Argentina");
INSERT INTO USERS (username, password, email, foto, descripcion, residencia)  VALUES ("Leo French", "$2y$10$eVpFabQtP8RtzuTq2FInuOjdiD5IUj6yZY8iNINXo7TsRPbohYkQ6", "colocai@mail.com", "", "Cada vez te quiero mas", "Villa Tesei, Buenos Aires, Argentina");
INSERT INTO USERS (username, password, email, foto, descripcion, residencia)  VALUES ("Jony Virginio", "$2y$10$eVpFabQtP8RtzuTq2FInuOjdiD5IUj6yZY8iNINXo7TsRPbohYkQ6", "jony@mail.com", "", "Soy modelo publicitario. Sí, en serio.", "Barrio Chino (Hurlingham), Buenos Aires, Argentina");
INSERT INTO USERS (username, password, email, foto, descripcion, residencia)  VALUES ("Cami Rosalinda (Chiki)", "$2y$10$eVpFabQtP8RtzuTq2FInuOjdiD5IUj6yZY8iNINXo7TsRPbohYkQ6", "cami@mail.com", "", "Vamos a volver", "En algún lugar del sur, Buenos Aires, Argentina");
INSERT INTO USERS (username, password, email, foto, descripcion, residencia)  VALUES ("El Oscarcito", "$2y$10$eVpFabQtP8RtzuTq2FInuOjdiD5IUj6yZY8iNINXo7TsRPbohYkQ6", "oscara@mail.com", "", "Miau <3", "Buenos Aires, Argentina");

INSERT INTO AMISTADES (requester_id, requested_id, estado) VALUES(1,2,"accepted");
INSERT INTO AMISTADES (requester_id, requested_id, estado) VALUES(1,3,"accepted");
INSERT INTO AMISTADES (requester_id, requested_id, estado) VALUES(1,4,"pending");
INSERT INTO AMISTADES (requester_id, requested_id, estado) VALUES(5,1,"pending");


INSERT INTO TIPOPOSTS (tipo) VALUES ("texto");
INSERT INTO TIPOPOSTS (tipo) VALUES ("foto");


INSERT INTO POSTS(autor_id, tipo_id, contenido) VALUES (1,1,"¡Qué lindo día hoy!");
INSERT INTO POSTS(autor_id, tipo_id, contenido) VALUES (1,1, "Estoy harta del mundial");
INSERT INTO POSTS(autor_id, tipo_id, contenido) VALUES (1,1, "Cada día me gusta más comer helado");
INSERT INTO POSTS(autor_id, tipo_id, contenido) VALUES (1,2, "Les presento al Oscarcito, no es hermoso?");
INSERT INTO POSTS(autor_id, tipo_id, contenido) VALUES (2,1, "No tengo más ganas de estudiar por hoy");
INSERT INTO POSTS(autor_id, tipo_id, contenido) VALUES (2,1, "Ví en el Día% unas papas tipo Pringles muy baratas. LAS RECOMIENDO");
INSERT INTO POSTS(autor_id, tipo_id, contenido) VALUES (2,1, "Alguien quiere ir a ver a Morrisey?");
INSERT INTO POSTS(autor_id, tipo_id, contenido) VALUES (2,2, "Tengo todo esto para leer");
INSERT INTO POSTS(autor_id, tipo_id, contenido) VALUES (3,1, "Hola hola hola");
INSERT INTO POSTS(autor_id, tipo_id, contenido) VALUES (3,2, "Aquí en la casa");

INSERT INTO IMAGENES(imagen, post_id)VALUES("oscarcito.jpg",4);
INSERT INTO IMAGENES(imagen, post_id)VALUES("libros.jpg",8);
INSERT INTO IMAGENES(imagen, post_id)VALUES("marcha.jpg",10);

INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(2,1, "Sí, hermoso");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(3,1, "Extrañaba el sol al mediodía");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(2,2, "Te entiendo amiga, un embole");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(3,2, "Qué mala onda!");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(2,2, "shhh, callese usted");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(2,3, "Sí, qué rico");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(2,4, "Lo amoooo!!!");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(3,4, "Ay es igual al mio!");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(1,5, "Mal hartísimos todos");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(3,5, "Ponele un poco más y ya te recibís");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(1,6, "Son buenísimas");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(3,7, "Vamos, quiero ir");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(1,8, "Wow un montón");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(3,8, "No es nada eso, no se quejen");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(1,9, "Yas!");
INSERT INTO COMENTARIOS (autor_id, post_id, comentario) VALUES(1,10, "Estuvo bueno?");

INSERT INTO FAVORITOS (post_id, user_id, has_modifications) VALUES(6,1,false);
INSERT INTO FAVORITOS (post_id, user_id, has_modifications) VALUES(7,1,false);
INSERT INTO FAVORITOS (post_id, user_id, has_modifications) VALUES(9,1,true);