<?php
namespace Social\Core;

use Exception;

/**
 * Clase que ejecuta la aplicación y le da funcionalidad
 */
class App
{
    protected static $rootPath;
    protected static $appPath;
    protected static $publicPath;
    protected static $viewsPath;
    protected static $urlPath;
	protected $controllerName;
	protected $controllerMethod;
    protected $request;
    protected $controller;
    protected $name;
    protected $method;
    protected $action;

    /**
     * Constructor
     *
     * @param string $rootPath
     */
    public function __construct($rootPath)
    {
        self::$rootPath = $rootPath;
        self::$appPath = $rootPath . "/app";
        self::$publicPath = $rootPath . "/public";
        self::$viewsPath = $rootPath . "/views";

        $url = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        self::$urlPath = $url;
    }

    /**
     * Arranca la app
     *
     * @return void
     */
    // public function run()
    // {
    //     $this->request = new Request;
    //     $method = $this->request->getMethod();
    //     $url = $this->request->getUrl();
    //     $url = substr($url, 10);  
      

    //     if (Route::exists($method, $url)) {
    //         $controller = Route::getController($method, $url);
    //         $action = Route::getAction($method, $url);
    //          $this->name = $controller;
    //          $this->action = $action;

    //         $this->runController($this->name, $this->action);
    //     } else {
    //         throw new Exception("URL inexistente");
    //     }
    // }

    public function run()
    {
        $this->request = new Request();
        $method = $this->request->getMethod();
        $url = $this->request->getUrl();
        $url = substr($url, 10);  
        
        // Verificamos si la ruta existe.
        if(Route::exists($method, $url)) {
            $controller = Route::getController($method, $url);
            $controllerParts = explode('@', $controller);
			$this->name   = $controllerParts[0];
			$this->method = $controllerParts[1];
            
            $this->executeController($this->name, $this->method);
        } else {
            throw new \Exception("No existe la ruta especificada.");
        }
    }

    /**
     * Corre el controlador
     *
     * @param string $controller
     * @param string $method
     * @return void
     */
     public function executeController($controller, $method)
	 {
         $controller = "\\Social\\Controllers\\" . $controller;
         $this->controller = new $controller;
         $this->controller->{$method}();
     }

  /*  public function executeController($controller)
    {
        $controllerData = explode('@', $controller);
        $controllerName = $controllerData[0];
        $controllerMethod = $controllerData[1];

        $controllerName = "\\Social\\Controllers\\" . $controllerName;
     echo $controllerName;
        $controllerObject = new $controllerName;

        $controllerObject->{$controllerMethod}();
    }
*/

    /**
     * Get the value of rootPath
     */ 
    public function getRootPath()
    {
        return self::$rootPath;
    }

    /**
     * Set the value of rootPath
     *
     * @return  self
     */ 
    public static function setRootPath($val)
    {
        self::$rootPath = $val;
    }

    /**
     * Get the value of appPath
     */ 
    public function getAppPath()
    {
        return $this->appPath;
    }

    /**
     * Set the value of appPath
     *
     * @return  self
     */ 
    public function setAppPath($appPath)
    {
        $this->appPath = $appPath;

        return $this;
    }

    /**
     * Get the value of publicPath
     */ 
    public function getPublicPath()
    {
        return self::$publicPath;
    }

    /**
     * Set the value of publicPath
     *
     * @return  self
     */ 
    public static function setPublicPath($val)
    { 
        self::$publicPath = $val;
    }

    /**
     * Get the value of viewsPath
     */ 
    public function getViewsPath()
    {
        return self::$viewsPath;
    }

    /**
     * Set the value of viewsPath
     *
     * @return  self
     */ 
    public function setViewsPath($val)
    {
        self::$viewsPath = $val;

    }

    /**
     * Get the value of urlPath
     */ 
    public function getUrlPath()
    {
        return self::$urlPath;
    }

    /**
     * Set the value of urlPath
     *
     * @return  self
     */ 
    public function setUrlPath($val)
    {
        self::$urlPath = $val;
    }
}
