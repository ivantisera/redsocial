(function () {

    'use strict';

    angular.module('social.services')
        .factory('PostsFactory', PostsFactory);
    function PostsFactory($http, API, Auth, Storage) {
        /**
         * Devuelve promesa para obtener todos los posts
         * 
         * @return promise
         */
        function getAllPosts() {
            return $http.get(API + '/fullposts', {
                headers: {
                    'X-Token': Auth.getToken()
                }
            });
        }

        /**
         * Devuelve promesa. Envío de comentario
         * 
         * @return promise
         */
        function sendComment(obj) {
            var promise = $http.post(API + "/sendcomment", obj, {
                headers: {
                    'X-Token': Auth.getToken()
                }
            });
            return promise;
        }


        /**
         * Devuelve promesa con todos los comentarios de un post
         * 
         * @param {string} id 
         */
        function getComments(id) {
            return $http.get(API + '/comments/' + id, {
                headers: {
                    'X-Token': Auth.getToken()
                }
            });
        }

        /**
         * Marca post como favorito
         * @param {string} id id del post
         * @return promise
         */
        function makeFavourite(id) {
            return $http.get(API + '/makefavourite/' + id, {
                headers: {
                    'X-Token': Auth.getToken()
                }
            });
        }

        /**
         * Elimina post de favoritos
         * @param {string} id id del post
         * @return promise
         */
        function deleteFavourite(id) {
            return $http.delete(API + "/deletefavourite/" + id, {
                headers: {
                    'X-Token': Auth.getToken()
                }
            });
        }

        /**
         * Eliminar marca de tiene novedades de oost favorito
         * @param {string} id id del post
         * @return promise
         */
        function markAsRead(id) {
            return $http.get(API + '/markasread/' + id, {
                headers: {
                    'X-Token': Auth.getToken()
                }
            });
        }
        


        /**
         * Prepara lista de posts a partir de la respuesta de la api para poder mostrarla y utilizarla por el controlador
         *
         * @param array obj
         * @return array
         */
        function formatObject(obj) {
 
            var posts = [];
            angular.forEach(obj, function (item) {
                var post = {};
                post.id = item.id;
                post.idusuario = item.autor_id;
                post.autor = item.username;
                post.contenido = item.contenido;
                post.fecha = item.fecha;
                post.media = null;
                post.addComment = false;
                post.viewComments = false;
                post.comments = [];
                post.isFav = item.isFav;

                posts.push(post);
            });
         
            return posts;
        }

        /**
         * Formatea objeto post al ser creado. Levanta los datos de la respuesta de la api
         * @param {Object} post 
         */
        function formatNewPost(post){
            return {
                id: post.id,
                idusuario: post.autor_id,
                autor: Storage.get('userInfo').username,
                contenido: post.contenido,
                fecha: "¡Nuevo!",
                media: null,
                addComment: false,
                viewComments: false,
                comments: [],
                isFav: false
            }
        }

        /**
         * Arma array de posteos favoritos para enviar a directiva y los muestre correctamente con las particularidades pertinentes
         * @param {array} favs Lista posts favoritos sin formatear
         * @returns {array} 
         */
        function formatFavourites(favs){
            var posts = [];
            angular.forEach(favs, function (item) {
                var post = {};
                post.id = item.id;
                post.idusuario = item.autor_id;
                post.autor = item.username;
                post.contenido = item.contenido;
                post.fecha = item.fecha;
                post.media = null;
                post.addComment = false;
                post.viewComments = false;
                post.comments = [];
                post.isFav = true;
                post.hasModifications = item.has_modifications == 1;

                posts.push(post);
            });
         
            return posts;
        }

        return {
            getAllPosts: getAllPosts,
            formatObject: formatObject,
            getComments: getComments,
            sendComment: sendComment,
            makeFavourite: makeFavourite,
            deleteFavourite: deleteFavourite,
            formatNewPost: formatNewPost,
            formatFavourites: formatFavourites,
            markAsRead: markAsRead
        };
    }

})();