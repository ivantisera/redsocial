(function () {

    'use strict';

    angular.module('social.services')
        .factory('Auth', Auth);
    function Auth($http, API, Storage, $state) {
        var token = null;
        var userInfo = {
            id: null,
            email: null
        }
        logFromStrorage();

        /**
         * Devuelve id de usuario si está logueado
         * @return {int}
         */
        function giveMeUser() {
            if (isLogged()) {
                return userInfo.id;
            }
            else return null;
        }

        /**
         * Intenta loguear al iniciar desde localstorage
         * @return void
         */
        function logFromStrorage() {
            if (Storage.has('token')) {
                token = Storage.get('token');
                userInfo = Storage.get('userInfo');
            }
        }

        /**
         * Devuelve el token
         */
        function getToken() {
            return Storage.get('token');
        };
        /**
         * Loguea al usuario
         * 
         * @param {user}  
         * @return promise
         */
        function login(user) {
            var promise = $http.post(API + "/login", user).then(function (response) {
               
                if (!angular.isUndefinedOrNullOrEmpty(response.data.data)) {
                    var data = response.data.data;
                    token = data.token;
                    userInfo.id = data.id;
                    userInfo.email = data.email;
                    userInfo.username = data.username;
                    Storage.set('token', token);
                    Storage.set('userInfo', userInfo);
                    return { status: "ok" };
                } else {
                    return { status: "error" };
                }
            }).catch(function (error) {
                return { status: "error" };
            });
            return promise;
        }

        /**
         * Limpia datos de Storage para logout.
         */
        function logout() {
            if (isLogged()) {
                token = null;
                Storage.remove("token");
                Storage.remove("userInfo");
                $state.go("login");
            }
        }

        /**
         * Chequea que el usuario esté logueado
         */
        function isLogged() {
            return token !== null;
        };



        /**
         * Crea nuevo usuario
         * @param {*} data 
         */
        function newUser(data) {
            return $http.post(API + "/register", data);
        }

        return {
            login: login,
            isLogged: isLogged,
            newUser: newUser,
            logout: logout,
            giveMeUser: giveMeUser,
            getToken: getToken
        };
    };





})();