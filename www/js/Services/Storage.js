angular.module('social.services')
    .factory('Storage', function () {

        /**
         * Obtiene variable de tipo string del localstorage y la devuelve parseada en json
         * @param string key
         * @return json
         * 
         */
        function get(key) {
            return JSON.parse(localStorage.getItem(key));
        }

        /**
         * Setea una variable en el localstorage
         * @param string key 
         * @param {value}
         * @return void 
         */
        function set(key, value) {
            localStorage.setItem(key, JSON.stringify(value));
        };


        /**
         * Devuelve si existe la key
         * @param string key 
         * @return boolean
         */
        function has(key) {
            return localStorage.getItem(key) !== null;
        }

        /**
         * Elimina una key del localstorage
         * @param string key 
         * @return void
         */
        function remove(key) {
            localStorage.removeItem(key);
        }

        return {
            get: get,
            set: set,
            has: has,
            remove: remove
        };


    });