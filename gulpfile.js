
var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var cleanCss = require('gulp-clean-css');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');



var paths = {
  sass: ['./scss/**/*.scss', './www/js/components/**/*.scss', './www/css/fonts/*.css'],
  js: ['./www/js/**/*.js', './www/js/components/**/*.js']
};

gulp.task('default', ['sass']);

gulp.task('js', function () {

  gulp
    .src(paths.js)
    .pipe(sourcemaps.init())
    .pipe(concat('bundle.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./www/dist/'));

});
/*
gulp.task('sass', function (done) {
  gulp.src(paths.sass)
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});*/

gulp.task('sass', function (done) {

  gulp
    .src(paths.sass)
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(concat('bundle.css'))
    .pipe(sourcemaps.write())
    .pipe(cleanCss({ keepSpecialComments: 0 }))
    .pipe(gulp.dest("./www/css/"))
    .on("end", done);

});

gulp.task('watch:js', ['js'], function () {
  gulp.watch(paths.js, ['js']);
});

gulp.task('watch:sass', ['sass'], function () {
  gulp.watch(paths.scss, ['sass']);
});

gulp.task('all:watch', ['js', 'sass'], function () {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.js, ['js']);
  

});
