<?php

namespace Social\DB;

use PDO;
use PDOStatement;

/**
 * Clase de conexión a la base de datos
 */
class DBConnection
{
    private static $db = null;

    /**
     * Constructor
     */
    private function __construct()
    {}

    /**
     * Devuelve la conexión de la base. Si no existe, la crea
     *
     * @return void
     */
    public static function getConnection()
    {
        if (!self::$db) {

            $server = "localhost:3306";
            $user = "root";
            $pass = "";
            $base = "SOCIAL_tisera_dw4atn";

            $dsn = "mysql:host=$server;dbname=$base;charset=utf8";

            try {
                self::$db = new PDO($dsn, $user, $pass);
                self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            } catch (Exception $e) {
                throw new Exception('Error de conexión con la base de datos');
            }

        }
        return self::$db;
    }

    /**
     * Devuelve el statement preparado 
     *
     * @param String $query
     * @return PDOStatement
     */
    public static function getSTMT($query)
    {
        return self::getConnection()->prepare($query);
    }
}
