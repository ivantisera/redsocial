(function () {
    'use strict';
    angular
        .module('social')
        .directive('inputValidator', inputValidator);
        
    /**
     * Directiva de que muestra / oculta errores de validaciones en inputs
     * Parámetros:
     *      model -> string | Valor que tiene el input
     *      type -> string | tipo a validar. De esto depende el error a mostrar y la validacion a realizar
     *      submitted -> boolean | Informa si el formulario ya fue submiteado
     *      required -> boolean | Si el campo es requerido
     */

    function inputValidator() {
        return {
            restrict: 'AE',
            templateUrl: './js/components/input-validator/input-validator.html',
            replace: true,
            scope: {
                model: "=",
                type: "=",
                submitted: "=",
                required: "=?"
            },
            link: function (scope) {
                var errors = {
                    "onlyrequired" : "Campo requerido. ",
                    "alphanumeric" : "No se permiten caracteres especiales. ",
                    "email" : "Formato de email inválido. ",
                    "onlynumbers" : "Sólo se permiten números. ",
                };

                var regExps = {
                    "alphanumeric" : /^([A-Za-z0-9\s\.\/\-\?\:\(\)\,\'\+])*$/,
                    "email" : /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    "onlynumbers" : /[^0-9]/,
                }
                
                /**
                 * Está mirando el model 
                 */
                scope.$watch('model', function(){
                    if(scope.submitted){
                        checkValidation();
                    }
                });

                scope.$watch('submitted', function(){
                    if(scope.submitted){
                        checkValidation();
                    }
                });

                scope.required = angular.isUndefinedOrNullOrEmpty(scope.required) ? false : scope.required;

                function checkValidation(){
                    scope.showError = false;
                    scope.errorMsg = "";
                    if(scope.required){
                        scope.showError = angular.isUndefinedOrNullOrEmpty(scope.model);
                        scope.errorMsg += errors.onlyrequired;
                    }

                    if(!angular.isUndefinedOrNullOrEmpty(scope.model)){
                        var regExp = regExps[scope.type];

                        var isValid = (regExp).test(scope.model);
                        if (!isValid){
                            scope.showError = true;
                            scope.errorMsg = errors[scope.type];
                        }
                    }
                }

            }
        }
    }
})();