<?php

namespace Social\Controllers;

use Social\Auth\Auth;
use Social\Core\Route;
use Social\Core\View;
use Social\Models\Post;
use Social\Models\User;
use Social\Models\Comment;
use Social\Storage\Session;
use Social\Validation\Validator;

/**
 * Clase que se encarga de la manipulación de comentarios realizados sobre los posts
 */
class CommentsController
{
    /**
     * Ejecuta la funcion estática getComments que le da los comentarios de un post para devolverlos al front
     */
    public function get()
    {
        $data = Route::getParams();
        $id = $data['id'];

        $error = "Error al obtener los comentarios. Por favor intenta nuevamente";

        $comment = Comment::getComments($id);

        View::responseToJSON($comment, $error);
    }

    /**
     * Valida y crea un comentario.
     */
    public function create()
    {
        Session::start();
        $error = [];
        $error['info'] = 'Error. No se puede crear el comentario';
        $input = file_get_contents('php://input');
        $input = json_decode($input, true);
        $token = Auth::getHeadersToken();

        if ($token !== null && Auth::validateToken($token)) {
            $post = Post::getPostFromId($input["post_id"]);
            $user = User::getUser(Session::get("id"));
            $error['validation'] = false;
            
            $validator = new Validator($input, [
                'comentario' => ['required'],
            ]);

            if ($validator->passes() && $post != null && $user != null){
                $comentario = Comment::create([
                    'autor_id' => $user->getId(),
                    'post_id' => $post->getId(),
                    'comentario' => $input["comentario"],
                ]);
                $comentario = [ "comentario" => $comentario];
                $post->modifyFavMark(true);

            } else {
                $comentario = null;
                $error['info'] = 'Revisar tu comentario';
                $error['validation'] = $validator->getErrors();
                $error = ["error" => $error];
            }

        }
        View::responseToJSON($comentario, $error);

    }

}
