<?php

namespace Social\Security;

use Exception;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Hmac\Sha256;

/**
 * Class Token
 * Manipulación de tokens
 */
class Token
{
  /**
   * Key estático para todos los token
   *
   * @var string
   */
  protected static $key = 'mfT5fsdnfsdJnJ0KLkefwlj89rwejkr6';

  /**
   * Quien emite el token
   *
   * @var string
   */
  protected static $issuer = 'http://ivantisera.google.com.ar/';

  /**
   * Crea un token
   *
   * @param String $id
   * @return String
   */
  public static function create($id)
  {
    $algoritmo = new Sha256;
    $builder = new Builder;

    $builder->setIssuer(self::$issuer);
    $builder->setIssuedAt(time());
    $builder->set('id', $id);
    $builder->sign($algoritmo, self::$key);

    $token = $builder->getToken();

    return (string) $token;
  }

 /**
  * Verifica la validez de un token
  *
  * @param String $token
  * @return array|void
  */
  public static function verify($token)
  {
    try {
      $algoritmo = new Sha256;
      $parser = new Parser;
      $token = $parser->parse($token);

      $validationData = new ValidationData;
      $validationData->setIssuer(self::$issuer);

      if(!$token->validate($validationData) || !$token->verify($algoritmo, self::$key)) {
        throw new Exception('El token no es válido');
      }

      return [
        'id' => $token->getClaim('id')
      ];
    }
    catch(Exception $e) {
      return false;
    }
  }

}