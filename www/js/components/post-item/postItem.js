(function () {
    'use strict';
    angular
        .module('social.directives')
        .directive('postItem', postItem);

        /**
         * Directiva que arma posteos. 
         * Interactúa con la API para agregar comentarios, marcar o eliminar de favoritos. Muestra/oculta comentarios, etc
         */
    function postItem() {
        return {
            restrict: 'AE',
            templateUrl: './js/components/post-item/post-item.html',
            replace: true,
            scope: {
                post: "=",
                deleteCallback: "=?",
                inFavs: "=?"
            },
            controller: function ($scope, PostsFactory, $ionicPopup, Storage, $state) {
                $scope.inFavs = angular.isUndefinedOrNullOrEmpty($scope.inFavs) ? false : $scope.inFavs;

                /**
                 * Marca un post como favorito
                 * @param {*} post 
                 */
                $scope.makeFav = function (post) {
                    PostsFactory.makeFavourite(post.id).then(response => {
                        if (response.data.status == 'ok') {
                            post.isFav = true;
                        }
                    })
                }

                /**
                 * Eliminar un post de favoritos. Si se pasó por parámetro a la directiva una función para ejecutar al eliminar, lo hace.
                 * Muentra un alert de confirmación
                 * @param {} post 
                 */
                $scope.delFav = function (post) {
                    $ionicPopup.alert({
                        'title': 'Eliminar de favoritos',
                        'template': '¿Seguro que querés eliminar este post de favoritos?'
                    }).then(() => {
                        PostsFactory.deleteFavourite(post.id).then(response => {
                            if (response.data.status == 'ok') {
                                post.isFav = false;
                                if ($scope.deleteCallback != null) { $scope.deleteCallback(post) }
                            }
                        });
                    });
                }

                /**
                 * LLama a la api al presionar boton comentar en cada posteo. Refresca.
                 * @param {obj} post 
                 * @param {string} comment 
                 */
                $scope.sendComment = function (post, comment) {
                    post.addComment = !post.addComment;

                    var obj = {
                        "post_id": post.id,
                        "comentario": angular.isDefined(comment) ? comment : ""
                    };

                    PostsFactory.sendComment(obj).then(function (resp) {
                        var response = resp.data;
                        post.viewComments = false;
                        if (response.status == "ok") {
                            $ionicPopup.alert({
                                'title': 'Perfecto',
                                'template': 'Comentario registrado'
                            });

                            response.data.comentario.fecha = "¡Nuevo!";
                            response.data.comentario.usuario = Storage.get('userInfo').username;
                            post.comments.push(response.data.comentario);

                        } else if (response.status == "error") {

                            angular.forEach(response.data.validation, function (item) {
                                angular.forEach(item, function (errorText) {
                                    vm.operations.errors.push(errorText);
                                })
                            });

                        }
                    }).catch(function () {
                        post.hasErrors = true;
                        post.errorTitle = "Error de carga. Por favor reintente";
                    }).finally(function () {
                        post.viewComments = true;
                    });

                }

                /**
                 * Trae los comentarios del post que se pasa como param
                 * @param {obj} post 
                 */
                $scope.getComments = function (post) {
                    PostsFactory.getComments(post.id).then(function (response) {
                        if (response.status == "error") {
                            post.hasError = true;
                            post.errorMsg = response.data;
                        }
                        else {
                            post.hasError = false;
                            post.comments = response.data.data.comments;
                        }
                    }).catch(function (error) {
                        post.hasError = true;
                        post.errorMsg = "Esta operación no puede ser realizada en este momento. Por favor reintente"
                    }).finally(() => {
                        post.viewComments = true;
                    });
                }

                /**
                 * Solo para posts que se muestran en la página de favoritos.
                 * Llama a la api para marcar novedades de un fav como leído.
                 */
                $scope.markAsRead = function(){
                    PostsFactory.markAsRead($scope.post.id).then(resp => {
                        if(resp.data.data){
                            $scope.post.hasModifications = false;
                        }
                    }) 
                }
            }
        }
    }
})();