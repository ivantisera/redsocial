(function () {

    'use strict';


    angular.module('social.controllers')
        .controller('EditController', EditController);

    function EditController($scope, $window, $state,  $stateParams, $ionicPopup, Auth, Storage, User, ProfileFactory) {
        var vm = this;

        var operations = {
            profile: {},
            submitted: false,
            hasErrors: false,
            errorTitle: "",
            errors: [],
            fileItem: {},
            file: "",
        };

        vm.operations = operations;
        vm.edit = edit;
        vm.setFile = setFile;


        init();

        /**
         * Se ejecuta al inicializar. Precarga los datos del perfil a editar o redirige a posts si viene vacio (cosa que nunca debería pasar)
         * 
         * @returns null
         * @author Iván Tisera
         */
        function init() {
           
            operations.profile = $stateParams.profile;
            if (angular.isUndefinedOrNullOrEmpty(operations.profile)) {
                $state.go('posts');
            }

        }



        /**
          * Llama a la api para ejecutar la edición recolectando la info cargada y muestra el alert ok. 
          * Si falla muestra los errores que vienen del backend
          * @returns null
         */
        function edit() {
            vm.operations.formSubmitted = true;
      
            var obj = {
                descripcion: vm.operations.profile.descripcion,
                email: vm.operations.profile.email,
                location: vm.operations.profile.residencia,
                name: vm.operations.profile.username,
                id: vm.operations.profile.id,
                foto: !angular.isUndefinedOrNullOrEmpty(vm.operations.fileItem) ? vm.operations.fileItem.file : "",
            };

            ProfileFactory.editprofile(obj).then(function (resp) {
                var response = resp.data;
                if (response.status == "ok") {
                    $ionicPopup.alert({
                        'title': 'Perfecto',
                        'template': '¡Perfil editado con éxito!'
                    }).then(function () {
                        var idProfile = vm.operations.profile.id;
                        $state.go("profile", { "id": idProfile }, { reload: true });

                    });
                } else if (response.status == "error") {

                    operations.hasErrors = true;
                    operations.errorTitle = response.data.info;
                    operations.errors = [];

                    angular.forEach(response.data.validation, function (item) {
                        angular.forEach(item, function (errorText) {
                            operations.errors.push(errorText);
                        })
                    });

                }
            });
        }

        /**
         * Guarda el archivo en base64 en los datos a enviar.
         * @param {file} fileUploaded Foto de perfil
         */
        function setFile(event) {
            var fileUploaded = event.target.files[0];
            if (checkError(fileUploaded)) {
                var reader = new FileReader();
                reader.readAsDataURL(fileUploaded);
                reader.onload = function (e) {
                    vm.operations.fileItem = {
                        file: reader.result,
                        name: fileUploaded.name
                    }
                }
            }
        }

        /**
         * Llamada desde setFile chequea que el archivo subido cumpla con los requisitos (tipo y tamaño) y muestra error en caso negativo
         * @param {file} file Foto de perfil
         */
        function checkError(file) {
            var maxSizeInKb = 1024;
            var maxLoadMsg = maxSizeInKb / 1024;
            var maxSize = maxSizeInKb * 1024;
            if (file.size > maxSize) {
                vm.operations.hasErrors = true;
                vm.operations.errorTitle = angular.isUndefinedOrNullOrEmpty(vm.operations.errorTitle) ? "Error en tu foto de perfil" : vm.operations.errorTitle;
                vm.operations.errors.push("El tamaño máximo de archivo es " + maxLoadMsg + "Mb.");
                $scope.$apply();
                return false;
            }
            else if (!isValidType(file.type)) {
                vm.operations.hasErrors = true;
                vm.operations.errorTitle = angular.isUndefinedOrNullOrEmpty(vm.operations.errorTitle) ? "Error en tu foto de perfil" : vm.operations.errorTitle;
                vm.operations.errors.push("Formato inválido de archivo.");
                $scope.$apply();
                return false;
            }
            else return true;
        }

        /**
         * Chequea que el tipo de archivo subido sea válido
         * @param {string} uploadedtype 
         */
        function isValidType(uploadedtype) {
            const filetypes = ["image/png", "image/jpeg", "application/pdf"];
            let isValid = false;
            angular.forEach(filetypes, (type) => {
                if (uploadedtype === type) {
                    isValid = true;
                }
            });
            return isValid
        }


    }
})();

