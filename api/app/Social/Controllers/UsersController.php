<?php

namespace Social\Controllers;

use Social\Auth\Auth;
use Social\Core\Route;
use Social\Core\View;
use Social\Models\Post;
use Social\Models\User;
use Social\Security\Hash;
use Social\Storage\Session;
use Social\Validation\Validator;

/**
 *
 */
class UsersController
{
    /**
     * Devuelve al servicio un usuario a partir del id
     *
     * @return void
     */
    public function get()
    {
        $error = "El perfil solicitado no existe.";
        $data = Route::getParams();
        $idIn = $data['id'];
        $token = Auth::getHeadersToken();

        if ($token !== null && Auth::validateToken($token)) {
         
            $profile = User::getUser($idIn);
            $id = $profile->getId();

            $friendshipStatus = $profile->getFriendshipStatus($id);
            $isFriend =  $friendshipStatus['status']  == "accepted";
            $owner = User::getUser(Session::get("id"));
            $userIsOwner = ($owner->getId() == $id);

            $posts = [];

            if ($isFriend || $userIsOwner) {
                $posts = Post::getUsersPosts($id,$owner->getId());
            }

            $friend = [];
            $requestedByMe = false;
            if ($userIsOwner) {
                $friend = 'myself';
            } else {
                $friend = $friendshipStatus['status'];
                if ($friend != 'none') {
                    $requestedByMe = $friendshipStatus['requester_id'] == Session::get("id");
                } else $requestedByMe = false;
            }

            $fullObject = ["profile" => $profile, "posts" => $posts, "friendship" => $friend, "requestedByMe" => $requestedByMe];
        }
        View::responseToJSON($fullObject, $error);
    }


    /**
     * Loguea un usuario
     *
     * @return void
     */
    public function login()
    {
        $error = "Error al registrar usuario";
        $input = file_get_contents('php://input');
        $input = json_decode($input, true);

        $email = $input["email"];
        $password = $input["password"];

        $error = "Usuario o contraseña inválidos";

        $user = Auth::login($email, $password);

        if ($user) {
            $login_data = [
                'token' => $user->getToken(),
                'id' => $user->getId(),
                'email' => $user->getEmail(),
                'username' => $user->getUsername()
            ];
        } else {
            $login_data = [];
        }
        View::responseToJSON($login_data, $error);
    }

    /**
     * Registra un nuevo usuario
     *
     * @return void
     */
    public function register()
    {
        $error = [];
        $error['info'] = 'Error. No se puede crear el perfil';
        $error['validation'] = false;
        $input = file_get_contents('php://input');
        $input = json_decode($input, true);

        $validator = new Validator($input, [
            'descripcion' => ['required', 'min:5'],
            'email' => ['required', 'email'],
            'location' => ['required', 'min:5', 'max:200'],
            'password' => ['required', 'min:6', 'max:8'],
            'name' => ['required', 'min:5', 'max:50'],
        ]);

        if ($validator->passes()) {
            $user = User::create([
                'username' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::secure($input['password']),
                'foto' => "",
                'descripcion' => $input['descripcion'],
                'residencia' => $input['location'],
            ]);
            if (!$user) {
                $user = [];
            }
            $error['info'] = 'El mail ya se encuentra registrado';
            $error['validation'] = false;
        } else {
            $user = null;
            $error['info'] = 'Revisar los campos';
            $error['validation'] = $validator->getErrors();
        }

        View::responseToJSON($user, $error);
    }

    /**
     * Edita un usuario
     *
     * @return void
     */
    public function edit()
    {
        $error = [];
        $error['info'] = 'Error. No se puede editar el perfil';
        $error['validation'] = false;
        $input = file_get_contents('php://input');

        $input = json_decode($input, true);

        $token = Auth::getHeadersToken();

        if ($token !== null && Auth::validateToken($token)) {

            $validator = new Validator($input, [
                'descripcion' => ['required', 'min:5'],
                'email' => ['required', 'email'],
                'location' => ['required', 'min:5', 'max:200'],
                'name' => ['required', 'alphanumeric', 'min:5', 'max:50'],
            ]);

            if ($validator->passes()) {

                $user = User::edit([
                    'id' => $input['id'],
                    'username' => $input['name'],
                    'email' => $input['email'],
                    'descripcion' => $input['descripcion'],
                    'residencia' => $input['location'],
                    'foto' => $input['foto'],
                ]);

                if (!$user) {
                    $user = [];
                } else {
                    $user = ["user" => "OK"];
                }
                $error['info'] = 'El mail ya se encuentra registrado';
                $error['validation'] = false;
            } else {
                $user = null;
                $error['info'] = 'Revisar los campos';
                $error['validation'] = $validator->getErrors();
            }
        }
        View::responseToJSON($user, $error);
    }


    /**
     * Genera el registro del pedido de amistad
     */
    public function requestFriendship()
    {
        $error = [];
        $error['info'] = 'Error.';
        $input = file_get_contents('php://input');
        $input = json_decode($input, true);

        $token = Auth::getHeadersToken();
        $status = 'none';
        if ($token !== null && Auth::validateToken($token)) {
            Session::start();
            $me = User::getUser(Session::get('id'));
            $friendToRequest = User::getUser($input['requested']);

            $status = $me->requestFriendship($friendToRequest->getId());
        }
        View::responseToJSON($status, $error);
    }


    /**
     * Rechaza amistad
     */
    public function rejectFriendship()
    {
        $input = Route::getParams();
        $this->modifyFriendship('reject', $input);
    }

    /**
     * Acepta amistad
     */
    public function acceptFriendship()
    {
        
        $input = file_get_contents('php://input');
        $input = json_decode($input, true);
        $this->modifyFriendship('accepted', $input);
    }

    /**
     * Modifica amistad (reutilizabñe)
     */
    function modifyFriendship($action, $input)
    {
        $error = [];
        $error['info'] = 'Error.';

        $token = Auth::getHeadersToken();
        $result = "";


        if ($token !== null && Auth::validateToken($token)) {
            Session::start();
            $me = User::getUser(Session::get('id'));
            $friendToModify = User::getUser($input['id']);

            $result = $me->modifyFriendship($action, $friendToModify->getId());
        }
        View::responseToJSON($result, $error);
    }

    /**
     * Trae notificaciones sobre solicitudes de amistad
     */
    public function getFriendRequests()
    {
        $error = [];
        $error['info'] = 'Error.';

        $input = file_get_contents('php://input');

        $input = json_decode($input, true);

        $token = Auth::getHeadersToken();
        $result = "";
        if ($token !== null && Auth::validateToken($token)) {
            Session::start();
            $me = Session::get('id');
            $profile = User::getUser($me);
            $result = $profile->getFriendRequests();
        }
        View::responseToJSON($result, $error);
    }

    /**
     * Trae los amigos de un perfil
     */
    function getFriends()
    {
        $token = Auth::getHeadersToken();
        $friends = "";
        $error = [];
        $error['info'] = 'Error.';
        Session::start();
        $me = Session::get('id');
        $profile = User::getUser($me);
        if ($token !== null && Auth::validateToken($token) && $profile != null) {
            $friends = $profile->getFriends();
        }
        View::responseToJSON($friends, $error);
    }

    function getProfileFriends()
    {
        $token = Auth::getHeadersToken();
        $data = Route::getParams();
        $id = $data['id'];
        $friends = "";
        $error = [];
        $error['info'] = 'Error.';

        $profile = User::getUser($id);
        
        if ($token !== null && Auth::validateToken($token) && $profile != null) {
            $friends = $profile->getProfileFriends();
        }
        View::responseToJSON($friends, $error);
    }

    /**
     * Activa notificación de favoritos
     */

    public function hasFavNews(){
        Session::start();
        $token = Auth::getHeadersToken();
        $resp = false;
        $error = "Error al obtener novedades";
        $profile = User::getUser(Session::get("id"));
        if ($token !== null && Auth::validateToken($token) && $profile != null) {
           $resp = $profile->hasFavNews();
        }
        View::responseToJSON($resp, $error);
    }

    /**
     * Devuelve favoritos
     */
    function getFavs(){
        $token = Auth::getHeadersToken();
        Session::start();
        $resp = false;
        $error = "Error al obtener novedades";
        $profile = User::getUser(Session::get("id"));
        if ($token !== null && Auth::validateToken($token) && $profile != null) {
           $resp = Post::getFullPosts(true, $profile->getId());
        }
        View::responseToJSON($resp, $error);
    }
}
