<?php
namespace Social\Auth;

use Social\Security\Hash;
use Social\Storage\Session;
use Social\Models\User;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Hmac\Sha256;

use Exception;


class Auth
{
	/**
	 * Ejecuta el login
	 * @param String $email Mail del usuario
	 * @param String $password Mail del usuario
	 * @return User 
	 */
	public static function login($email, $password)
	{
	
		$user = User::getUserByEmail($email);		
		if($user !== null) {

			if(Hash::check($password, $user->getPassword())) {

				$token = self::createToken($user->getId());
				$user->setToken($token);
				Session::start();
				self::logUser($user->getId());
				return $user;
			}
		}
		return false;
	}


	/**
	 * Crea un token para el usuario del id que viene por parametro
	 *
	 * @param String $id
	 * @return String 
	 */
	public static function createToken($id){
		$algoritm = new Sha256();
		$builder = new Builder;
		
		$builder->setIssuer("http://ivantisera.com");
		$builder->setIssuedAt(time());
		$builder->set('id', $id);
		$builder->sign($algoritm, "fsdf45dsf4sd5fsd5fsd12fsd");
		
		$token = $builder->getToken();
		
		return (string) $token;
	}

	
	/**
	 * Valida token
	 */
	public static function validateToken($token)
		{
			try {
				$parser = new Parser;
				$algoritm = new Sha256;
				$token = $parser->parse($token);

				$valData = new ValidationData;
				$valData->setIssuer("http://ivantisera.com");

				if(!$token->validate($valData)) {
					throw new Exception("Error de token");
				}

				if(!$token->verify($algoritm, "fsdf45dsf4sd5fsd5fsd12fsd")) {
					throw new Exception("Error de token");
				}

				return [
					'id' => $token->getClaim('id')
				];
			} catch(\Exception $e) {
				return false;
			}
		}
	
	/**
	* Levanta el token
	* @return string $token
	*/
	
	public static function getHeadersToken(){
		
		if(isset($_SERVER['HTTP_X_TOKEN'])){
			$token = $_SERVER['HTTP_X_TOKEN'];
		}else{
			$token = null;
		}
		return $token;
	}
	
	/**
	 * Marca al usuario como logueado.
	 *
	 * @param User $user
	 */
	public static function logUser($id)
	{
		Session::set('id', $id);
	}

	/**
	 * Obtiene el id del usuario logueado.
	 *
	 * @return int
	 */
	public static function getUserId()
	{
		return (int) Session::get('id');
	}
	
}