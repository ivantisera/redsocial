(function () {
    'use strict';
    angular
        .module('social.directives')
        .directive('friendItem', friendItem);
        /**
         * Directiva que arma componente amigo
         * 
         */
    function friendItem() {
        return {
            restrict: 'AE',
            templateUrl: './js/components/friend-item/friend-item.html',
            replace: true,
            scope: {
                item: "="
            },
            controller: function ($scope) {
                $scope.item.foto = angular.isUndefinedOrNullOrEmpty($scope.item.foto) ? './img/avatar.jpg' : $scope.item.foto;
            }
        }
    }
})();