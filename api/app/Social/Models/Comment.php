<?php
namespace Social\Models;
use Social\DB\DBConnection;
use PDO;
use JsonSerializable;

class Comment extends Model implements JsonSerializable
//class Comment 
{
	protected $table 		= "comentarios";
	protected $primaryKey 	= "id";
	protected $attributes 	= ['id', 'autor_id', 'post_id', 'comentario', 'fecha'];


    protected $id;
    protected $autor_id;
    protected $post_id;
    protected $comentario;
    protected $fecha;
    
	public function JsonSerialize()
	{
		return [
			'id' 	        => $this->id,
			'autor_id' 		=> $this->autor_id,
			'post_id' 		=> $this->post_id,
			'comentario' 	=> $this->comentario,
			'fecha' 		=> $this->fecha,
		];
	}
	
	public static function getComments($id)
	{
		$query = "SELECT c.id, c.autor_id, c.post_id, c.comentario, c.fecha, u.username as usuario FROM comentarios c JOIN users u on c.autor_id = u.id where c.post_id =?";
		
        $stmt = DBConnection::getSTMT($query);

		$stmt->execute([$id]);

		$result = [];

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$result[] = $row;
		}
		
		$result = [ "comments" => $result];
		return $result;
	}
}