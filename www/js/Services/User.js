(function () {

    'use strict';

    angular.module('social.services')
        .factory('User', User);
    function User($http, API, Storage, Auth) {
        var token = null;
        var userInfo = {
            id: null,
            email: null
        };

        /**
         * Setter de token
         * @param {string} tok 
         */
        function setToken(tok) {
            token = tok;
        }

        /**
         * Getter de token
         * @return string
         */
        function getToken() {
            return token;
        }


        /**
         * Setter de informacion de usuario
         * @param {obj} usr 
         */
        function setUserInfo(usr) {
            userInfo = usr;
        }

        /**
         * Getter de usuario
         * return obj
         */
        function getUserInfo() {
            return userInfo;
        }


        /**
         * Elimina el log
         * 
         */
        function logout() {
            if (Storage.has("token")) {
                Storage.remove("token");
                Storage.remove("userInfo");
            }
        }

        function newUser(data) {
            var promise = $http.post(API + "/register", data);
            return promise;
        }

        return {
            logout: logout,
            newUser: newUser,
            setToken: setToken,
            getToken: getToken,
            setUserInfo: setUserInfo,
            getUserInfo: getUserInfo

        };
    }

})();