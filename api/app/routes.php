<?php
use Social\Core\Route;


//Route::create('GET', '/allposts', 'PostsController@index');
Route::create('GET', '/fullposts', 'PostsController@full');
Route::create('GET', '/profile/{id}', 'UsersController@get');
Route::create('GET', '/getposts/{id}', 'UsersController@getPosts');
Route::create('POST', '/login', 'UsersController@login');
Route::create('GET', '/comments/{id}', 'CommentsController@get');
Route::create('POST', '/register', 'UsersController@register');
Route::create('POST', '/sendpost', 'PostsController@create');
Route::create('POST', '/sendcomment', 'CommentsController@create');
Route::create('POST', '/editprofile', 'UsersController@edit');
Route::create('POST', '/requestfriendship', 'UsersController@requestfriendship');
Route::create('PUT', '/acceptfriendship', 'UsersController@acceptFriendship');
Route::create('DELETE', '/rejectfriendship/{requester}', 'UsersController@rejectFriendship');
Route::create('GET', '/getfriendrequests', 'UsersController@getFriendRequests');
Route::create('GET', '/getfriends', 'UsersController@getFriends');
Route::create('GET', '/getprofilefriends/{id}', 'UsersController@getProfileFriends');
Route::create('GET', '/makefavourite/{id}', 'PostsController@makeFavourite');
Route::create('GET', '/hasfavnews', 'UsersController@hasFavNews');
Route::create('GET', '/favourites', 'PostsController@getFavs');
Route::create('GET', '/markasread/{id}', 'PostsController@markAsRead');
Route::create('DELETE', '/deletefavourite/{id}', 'PostsController@deleteFavourite');



