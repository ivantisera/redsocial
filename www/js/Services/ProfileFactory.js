(function () {

    'use strict';

    angular.module('social.services')
        .factory('ProfileFactory', ProfileFactory);
    function ProfileFactory($http, API, Auth) {

        /**
         * Devuelve promesa con llamada a la api para traer datos de un perfil
         * @param {string} id Id del usuario
         * @returns {promise}
         */
        function getProfile(id) {
            return $http.get(API + '/profile/' + id, {headers: {
                'X-Token':  Auth.getToken()
              }});
        }

        /**
         * Devuelve promesa con llamada a la api para traer novedades sobre solicitudes de amistad
         * @returns {promise}
         */
        function getFriendsRequests() {
            return $http.get(API + '/getfriendrequests', {headers: {
                'X-Token':  Auth.getToken()
              }});
        }

        /**
         * Devuelve promesa con llamada a la api para traer amigos de un perfil
         * @param {string} id Id del usuario a consultar
         */
        function getFriendList() {
            return $http.get(API + '/getfriends', {headers: {
                'X-Token':  Auth.getToken()
              }});
        }

        /**
         * Devuelve promesa con llamada a la api para traer amigos/solicitudes/sugerencias
         * @param {string} id Id del usuario a consultar
         */
        function getProfileFriends(id) {
            return $http.get(API + '/getprofilefriends/' + id, {headers: {
                'X-Token':  Auth.getToken()
              }});
        }

        /**
         * Envía comentario a un post
         * @param {Object} obj Comentario
         * @returns {promise}
         */
        function sendPost(obj) {
            var promise = $http.post(API + "/sendpost", obj, {headers: {
                'X-Token':  Auth.getToken()
              }});
            return promise;
        }

        /**
         * Devuelve promesa con llamada a la api para aceptar solicitud de amistad
         * @param {Object} obj Objeto user con los parametros a editar
         * @returns {promise}
         */
        function editprofile(obj) {
            var promise = $http.post(API + "/editprofile", obj, {headers: {
                'X-Token':  Auth.getToken()
              }});
            return promise;
        }

        /**
         * Devuelve promesa con llamada a la api para solicitar amistad
         * @param {Object} obj  ID del usuario al que se le solicita amistad
         * @returns {promise}
         */
        function requestFriendship(obj){
            var promise = $http.post(API + "/requestfriendship", obj, {headers: {
                'X-Token':  Auth.getToken()
              }});
            return promise;
        }

        /**
         * Devuelve promesa con llamada a la api para aceptar solicitud de amistad
         * @param {Object} obj ID del user a aceptar
         * @returns {promise}
         */
        function acceptFriendship(obj){
            var promise = $http.put(API + "/acceptfriendship", obj, {headers: {
                'X-Token':  Auth.getToken()
              }});
            return promise;
        }

        /**
         * Devuelve promesa con llamada a la api para eliminar solicitud / rechazar
         * @param {Object} obj ID del usuario a rechazar / eliminar solicitud
         * @returns 
         */
        function rejectFriendship(obj){
            var promise = $http.delete(API + "/rejectfriendship/"+ obj.requester, {headers: {
                'X-Token':  Auth.getToken()
              }});
            return promise;
        }

        /**
         * Busca novedades en los favoritos
         * 
         * @returns {promise}
         */
        function hasFavNews() {
            return $http.get(API + '/hasfavnews', {
                headers: {
                    'X-Token': Auth.getToken()
                }
            });
        }

         /**
         * Devuelve promesa con llamada a la api para traer Posts marcados como favoritos
         * 
         * @returns {promise}
         */
        function getFavs(){
            return $http.get(API + '/favourites', {
                headers: {
                    'X-Token': Auth.getToken()
                }
            });
        }

        return {
            acceptFriendship: acceptFriendship,
            rejectFriendship: rejectFriendship,
            getProfile: getProfile,
            sendPost : sendPost,
            editprofile : editprofile,
            requestFriendship: requestFriendship,
            getFriendsRequests: getFriendsRequests,
            getFriendList: getFriendList,
            getProfileFriends: getProfileFriends,
            hasFavNews: hasFavNews,
            getFavs: getFavs
        };
    }

})();