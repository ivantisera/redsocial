(function () {
    'use strict';
    angular
        .module('social.directives')
        .directive('nothingHere', nothingHere);

    /**
     * Directiva que arma la pantalla cuando no hay datos para mostrar.
     */
    function nothingHere() {
        return {
            restrict: 'AE',
            templateUrl: './js/components/nothing-here/nothing-here.html',
            replace: true,
            transclude:true,
            scope: {
                title: '=?'
            },
            controller: function ($scope) {
                $scope.title = angular.isUndefinedOrNullOrEmpty($scope.title) ? "Ups, nada por acá" : $scope.title;
            }
        }
    }
})();