
(function () {
    'use strict';

    angular
        .module('social')
        .config(socialRouter);
    /**
     * Arma el ruteo de la app
     * @param {} $stateProvider 
     * @param {*} $urlRouterProvider 
     */
    function socialRouter($stateProvider, $urlRouterProvider) {
        $stateProvider
        .state('posts', {
          url: '/posts',
          templateUrl: 'templates/posts.html',
          controller: 'PostsController as postsTab',
          data: {
            requireAuth: true
          },
        })
        .state('profile', {
          url: '/profile/:id',
          templateUrl: 'templates/profile.html',
          controller: 'ProfileController as profile',
          data: {
            requireAuth: true
          }
        })
        .state('editprofile', {
          url: '/editprofile',
          templateUrl: 'templates/edit-profile.html',
          controller: 'EditController as edit',
          data: {
            requireAuth: true,
          }, 
          params: {
            profile: {}
          }
        })
        .state('seefriends', {
          url: '/seefriends/:id',
          templateUrl: 'templates/see-friends.html',
          controller: 'friendListCtrl as friendsCtrl',
          data: {
            requireAuth: true
          }
        })
        .state('myfavourites', {
          url: '/myfavourites/:id',
          templateUrl: 'templates/favourites.html',
          controller: 'favouritesCtrl as favCtrl',
          data: {
            requireAuth: true
          }
        })
        .state('login', {
          url: '/login',
          templateUrl: 'templates/login.html',
          controller: 'LoginController as loginCtrl',
          data: {
            requireAuth: false
          }
        })
        .state('register', {
          url: '/register',
          templateUrl: 'templates/register.html',
          controller: 'RegisterController as RegisterCtrl',
          data: {
            requireAuth: false
          }
        });

    // Página por defecto
    $urlRouterProvider.otherwise('login');
    }
})();