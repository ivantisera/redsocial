<?php

namespace Social\Security;

/**
 * CLass Hash
 * Manipula el encriptado de las contraseñas
 */
class Hash{
    /**
     * Devuelve una password hasheada
     *
     * @param String $pass_in
     * @return String 
     */
    public static function secure($pass_in)
    {
        $pass_out = password_hash($pass_in, PASSWORD_DEFAULT);
        return $pass_out;
    }
    
    /**
     * Valida la contraseña hasheada
     *
     * @param String $pass_in
     * @param String $pass_secured
     * @return boolean
     */
    public static function check($pass_in, $pass_secured)
    {
        $is_valid = password_verify($pass_in, $pass_secured);
        return $is_valid;

       // return true;
    }
}

?>