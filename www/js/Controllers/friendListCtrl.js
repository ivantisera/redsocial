(function () {

    'use strict';


    angular.module('social.controllers')
        .controller('friendListCtrl', friendListCtrl);

    function friendListCtrl($scope, ProfileFactory, $stateParams, Storage, $state, $ionicPopup, Auth, User) {
        var vm = this;
        var titles = ["Solicitudes pendientes ", "Le pediste solicitud a", "Son amigos", "Sugerencias de amistad"]
        var types = ["toAccept", "requestedByMe", "myFriends", "suggestions"];

        var operations = {
            loading: false,
            hasError: false,
            errorMsg: "Hubo un error al traer tus amigos. Reintentá luego.",
            profileOwner: "",
            me: Storage.get('userInfo'),
            friends: [],
            profileNoFriendsMsg: false,
            noFriendTitle: "Ups, este perfil no tiene amigos."
        };

        vm.operations = operations;

        init();

        function init() {
            operations.profileOwner = $stateParams;
            loadFriendList();
        }

        /**
         * Llama a la Api y carga listas de amigos.
         * Da formato a objetos y arma las listas por estado.
         */
        function loadFriendList() {
            operations.hasError = false;
            operations.loading = true;
 
            if (operations.me.id == operations.profileOwner.id) {
                ProfileFactory.getFriendList().then(response => {
                    var list = [];
                    var data = response.data.data;
                    angular.forEach(titles, (title, key) => {
                        switch (key) {
                            case 0: list = data.toAccept; break;
                            case 1: list = data.requestedByMe; break;
                            case 2: list = data.friends; break;
                            case 3: list = data.suggestions; break;
                        };
                        operations.friends[key] = {
                            list: list,
                            hasItems: list.length > 0,
                            title: title,
                            type: types[key]
                        };
                    })
                }).catch(err => {
                    operations.hasError = true;
                }).finally(() => {
                    operations.loading = false;
                });
            }
            else {
                ProfileFactory.getProfileFriends(operations.profileOwner.id).then(response => {
                    operations.friends[0] = {
                        list: response.data.data,
                        hasItems: response.data.data.length > 0,
                        title: "",
                        type: 'profileFriends'
                    }
                    if(!operations.friends[0].hasItems) operations.profileNoFriendsMsg = true;
                }).catch(err => {
                    operations.hasError = true;
                }).finally(() => {
                    operations.loading = false;
                    console.log(operations);
                });
            }
        }


    }
})();

