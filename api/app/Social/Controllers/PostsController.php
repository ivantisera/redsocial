<?php

namespace Social\Controllers;

use Social\Auth\Auth;
use Social\Core\View;
use Social\Core\Route;
use Social\Storage\Session;
use Social\Models\User;
use Social\Models\Post;
use Social\Validation\Validator;

/**

 *
 */
class PostsController
{
  
    public function getPosts($onlyfavs)
    {
        $error = "Ha ocurrido un error. Por favor reintente luego";
        $token = Auth::getHeadersToken();

        Session::start();
        $profile = User::getUser(Session::get("id"));
      
        if ($token !== null && Auth::validateToken($token) && $profile != null) {
            if($onlyfavs) {
                $posts = Post::getFavPosts($profile->getId());
            }else{
                $posts = Post::getFullPosts($profile->getId());
            }
            
        }
        View::responseToJSON($posts, $error);
    }

    /**
     * Llama a la funcion propia getPosts. 
     * Pide todos los post propios y de amigos
     */
    public function full(){
        $this->getPosts(false);
    }

    /**
     * Llama a la funcion propia getPosts. 
     * Pide los post favoritos propios y de amigos
     */
    public function getFavs(){
        $this->getPosts(true);
    }

    public function create()
    {
        $error = [];
        $error['info'] = 'Error. No se puede crear el post';
        $error['validation'] = false;
        $input = file_get_contents('php://input');

        $token = Auth::getHeadersToken();

        if ($token !== null && Auth::validateToken($token)) {

            $input = json_decode($input, true);
            $validator = new Validator($input, [
                'contenido' => ['required'],
            ]);

            if ($validator->passes()) {
                $post = Post::create([
                    'autor_id' => $input['autor'],
                    'contenido' => $input["contenido"],
                    'tipo_id' => "1",
                ]);
                $post = ["post" => $post];
            } else {
                $post = [];
                $error['info'] = 'Revisar los campos';
                $error['validation'] = $validator->getErrors();
                $error = ["error" => $error];
            }
        }
        View::responseToJSON($post, $error);
    }

    /**
     * Marca post como fav
     */
    public function makefavourite(){
        $resp = false;
        $error = "Error al marcar favorito";
        $data = Route::getParams();
        $token = Auth::getHeadersToken();
        Session::start();
        $post_id = $data['id'];
        $user_id = Session::get("id");
        if ($token !== null && Auth::validateToken($token)) {
            $post = Post::getPostFromId($post_id);
            $profile = User::getUser($user_id);
            $resp = $post->markAsFavouriteOf($profile->getId());
        }
        View::responseToJSON($resp, $error);
    }

    /**
     * Elimina favorito
     */
    public function deletefavourite(){
        $resp = false;
        $error = "Error al eliminar favorito";
        $data = Route::getParams();
        $token = Auth::getHeadersToken();
        Session::start();
        $post_id = $data['id'];
        $user_id = Session::get("id");
        if ($token !== null && Auth::validateToken($token)) {
            $post = Post::getPostFromId($post_id);
            $profile = User::getUser($user_id);
            $resp = $post->deleteFavouriteOf($profile->getId());
        }
        View::responseToJSON($resp, $error);
    }

    /**
     * Marca favorito como leido
     */
    public function markAsRead(){
        $resp = false;
        $error = "Error al marcar favorito";
        $data = Route::getParams();
        $token = Auth::getHeadersToken();
        Session::start();
        $post_id = $data['id'];
        $user_id = Session::get("id");
        if ($token !== null && Auth::validateToken($token)) {
            $post = Post::getPostFromId($post_id);
            $profile = User::getUser($user_id);
            $resp = $post->markAsRead($profile->getId());
        }
        View::responseToJSON($resp, $error);
    }

}
