(function () {

    'use strict';


    angular.module('social.controllers')
        .controller('RegisterController', RegisterController);

    function RegisterController($scope, $state, $ionicPopup, Auth, Storage, User) {
        var vm = this;
        var operations = {
            hasErrors: false,
            errors: [],
            errorTitle: ""
        };

        vm.operations = operations;
        vm.go = go;


        init();

        function init() {
            vm.operations.errors = [];
            User.logout();
        }

        /**
         * Llama a la api para registrar usuario. Setea errores de haberlos
         * @param {obj} data 
         */
        function go(data) {
            var obj = {
                descripcion: "",
                email: "",
                location: "",
                name: "",
                password: ""
            };

            if (angular.isDefined(data)) {
                var obj = {
                    descripcion: angular.isDefined(data.descripcion) ? data.descripcion : "",
                    email: angular.isDefined(data.email) ? data.email : "",
                    location: angular.isDefined(data.location) ? data.location : "",
                    name: angular.isDefined(data.name) ? data.name : "",
                    password: angular.isDefined(data.password) ? data.password : ""
                }
            }

            User.newUser(obj).then(function (resp) {
                var response = resp.data;
              
                if (response.status == "ok") {
                    var popup = $ionicPopup.alert({
                        'title': 'Bienvenido',
                        'template': 'Registro exitoso'
                    });
                    popup.then(function () {
                        $state.go('login');
                    });
                } else if (response.status == "error") {

                    vm.operations.hasErrors = true;
                    vm.operations.errorTitle = response.data.info;
                    vm.operations.errors = [];

                    angular.forEach(response.data.validation, function (item) {
                        angular.forEach(item, function (errorText) {
                            vm.operations.errors.push(errorText);
                        })
                    });

                }
            });
        }
    }
})();



