<?php

namespace Social\Models;

use Exception;
use PDO;
use PDOStatement;
use Social\DB\DBConnection;

/**
 * Clase base de objetos para conexión con BD
 */
class Model
{
    /**
     * Tabla de base de datos
     *
     * @var string
     */
    protected $table = "";

    /**
     * Primary Key
     *
     * @var string
     */
    protected $primaryKey = "";

    /**
     * Array de atributos de la tabla
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Constructor genérico
     *
     * @param int|null $id
     */
    public function __construct($id = null)
    {
        if (!is_null($id)) {
            $this->getByPk($id);
        }
    }

    /**
     * Trae un registro a partir de la pk
     *
     * @param [type] $id
     * @return void
     */
    public function getByPk($id)
    {
        $db = DBConnection::getConnection();
        $query = "SELECT * FROM " . $this->table . "
				WHERE " . $this->primaryKey . " = ?";
        $statement = $db->prepare($query);

        $success = $stmt->execute([$pk]);

        if ($success) {
			$this->loadDataFromRow($statement->fetch(PDO::FETCH_ASSOC));
			
        } else {
            throw new Exception("Error en la creación del registro");
        }
    }

    /**
     * Arma la row por atributos pedidos
     *
     * @param array $data
     * @return void
     */
    public function loadDataFromRow($data)
    {
        foreach ($this->attributes as $attr) {
            if (isset($data[$attr])) {
                $this->{$attr} = $data[$attr];
            }
        }
    }

    /**
     * Trae todos los registros de la tabla
     *
     * @return array
     */
    public static function getAll()
    {
        $self = new static;
        $query = "SELECT * FROM " . $self->table;

        $stmt = DBConnection::getSTMT($query);

        $stmt->execute();

        $result = [];

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $obj = new static;
            $obj->loadDataFromRow($row);
            $result[] = $obj;
        }
        return $result;
    }

/**
 * Crea e inserta nuevo registro
 *
 * @param array $data
 * @return void
 */
    public static function create($data)
    {
		
        $query = self::createInsert($data);
        $insertData = self::filter($data);

       

        $db = DBConnection::getConnection();

            $stmt = DBConnection::getSTMT($query);
			$exito = $stmt->execute($insertData);


        if ($exito) {
			$obj = new static;
			$obj->loadDataFromRow($data);
			$obj->id = $db->lastInsertId();

            return $obj;
        } else {
            return false;
		}
		
    }

    /**
     * Arma automáticamente la query de insert
     *
     * @param array $data
     * @return String
     */
    public static function createInsert($data)
    {

        $self = new static;

        $query = "INSERT INTO " . $self->table . " (";

        $campos = [];
        $datos = [];

        foreach ($data as $nombre => $valor) {
            if (in_array($nombre, $self->attributes)) {
                $campos[] = $nombre;
                $datos[] = ":" . $nombre;
            }
        }

        $query .= implode(",", $campos) . ")";

        $query .= " VALUES (" . implode(",", $datos) . ")";
      //  var_dump($query);
        return $query;
    }

    /**
     * Limpia el array de datos no reconocidos como válidos.
     *
     * @param array $data
     * @return array
     */
    public static function filter($data)
    {
        $self = new static;
        $response = [];

        foreach ($data as $nombre => $valor) {
            if (in_array($nombre, $self->attributes)) {
                $response[$nombre] = $valor;
            }
        }

        return $response;
    }

    /**
     * Get primary Key
     *
     * @return  string
     */ 
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * Set primary Key
     *
     * @param  string  $primaryKey  Primary Key
     *
     * @return  self
     */ 
    public function setPrimaryKey(string $primaryKey)
    {
        $this->primaryKey = $primaryKey;

        return $this;
    }
}
