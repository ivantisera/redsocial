(function () {

    'use strict';

    angular.module('social.controllers')
        .controller('favouritesCtrl', favouritesCtrl);

    function favouritesCtrl($scope, ProfileFactory, PostsFactory, $stateParams, Storage, $state, $ionicPopup, Auth, User) {
        var vm = this;
        var operations = {
            hasErrors: false,
            errorMsg: '',
            loading: false,
            favs: {
                list: [],
                hasFavs: true,
            },
            titleNoData: "No tenés favoritos"
        }

        vm.operations = operations;
        vm.deleteCallback = deleteCallback;
        vm.goToMyProfile = goToMyProfile;
        vm.exit = exit;

        init();

        function init(){
            loadFavs();
        }

        /**
         * Llama a la API para cargar los post favoritos. 
         * Llena el array operations.fav.list
         */
        function loadFavs(){
            ProfileFactory.getFavs().then(resp => {
                operations.favs.list = PostsFactory.formatFavourites(resp.data.data);
            });
        }

        /**
         * Se ejecuta al eliminar un post de favoritos. Se pasa por parámetro al componente post-item para ser ejecutado por éste.
         * @param {Object} post 
         */
        function deleteCallback(post){
            operations.favs.list = operations.favs.list.filter(fav => fav.id != post.id);
        }

         /**
         * Redireccion al perfil del usuario
         */
        function goToMyProfile() {
            var id = Storage.get("userInfo").id;
            $state.go("profile", { "id": id });
        }

        /**
         * Funcion que ejecuta el logout
         */
        function exit() {
            Auth.logout;
            $state.go("login");
        }

    }
})();