<?php

namespace Social\Models;

use Social\DB\DBConnection;
use PDO;
use JsonSerializable;
use Social\Storage\Session;

class User extends Model implements JsonSerializable
{
    protected $id;
    protected $username;
    protected $email;
    protected $password;
    protected $foto;
    protected $descripcion;
    protected $residencia;
    protected $token;

    protected $table = "users";
    protected $primaryKey = "id";
    protected $attributes = ['id', 'username', 'email', 'password', 'foto', 'descripcion', 'residencia'];

    public function JsonSerialize()
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'email' => $this->email,
            'password' => $this->password,
            'foto' => $this->foto,
            'descripcion' => $this->descripcion,
            'residencia' => $this->residencia,
        ];
    }

    /**
     * Devuelve el usuario con todos sus datos para su visualizacion ocultando la password
     *
     * @param string $id
     * @return $user
     */
    public static function getUser($id)
    {

        $query = "SELECT * FROM users WHERE id = ?";

        $stmt = DBConnection::getSTMT($query);

        if ($stmt->execute([$id])) {
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $user = new static;
                $user->loadDataFromRow($row);
                $user->password = "null";
                return $user;
            }
        } else {
            return null;
        }
    }

    
    /**
     * edit Graba la edición en la BD
     *
     * @param mixed $data 
     * @return true|null
     */
    public static function edit($data)
    {
        $db = DBConnection::getConnection();
        $query = "UPDATE users
                    SET username= :username, EMAIL= :email, DESCRIPCION= :descripcion, RESIDENCIA= :residencia";
        if (strlen($data['foto']) > 0) {
            $query = $query . ", FOTO= :foto";
        }
        $query = $query . " WHERE ID = :id";

        $stmt = DBConnection::getSTMT($query);

        $params = [
            'username' => $data['username'],
            'email' => $data['email'],
            'descripcion' => $data['descripcion'],
            'residencia' => $data['residencia'],
            'id' => $data['id']
        ];

        if (strlen($data['foto']) > 0) {
            $foto = ['foto' => $data['foto']];
            $params = array_merge($params, $foto);
        }

        $exito = $stmt->execute($params);

        if ($exito) {
            return true;
        } else {
            return null;
        }
    }

    
    /**
     * getUserByEmail Crea instancia usuario a partir del correo
     *
     * @param  mixed $email
     * @return user|null
     */
    public static function getUserByEmail($email)
    {

        $query = "SELECT * FROM users WHERE email = ?";

        $stmt = DBConnection::getSTMT($query);

        if ($stmt->execute([$email])) {
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $user = new static;
                $user->loadDataFromRow($row);
                return $user;
            }
        } else {
            return null;
        }
    }
    
    /**
     * getFriendshipStatus Estado de amistad
     *
     * @param  mixed $friend_id
     * @return void
     */
    public function getFriendshipStatus($friend_id)
    {
        Session::start();
      
        $me = Session::get('id');
        $query = "SELECT * FROM amistades WHERE requester_id IN (:me, :friend_id) AND requested_id IN (:me, :friend_id)";

        $stmt = DBConnection::getSTMT($query);

        $params = [
            'me' => $me,
            'friend_id' => $friend_id
        ];

        $exito = $stmt->execute($params);
        $friendshipStatus = [];
        if ($exito) {
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $friendshipStatus['status'] = $row['estado'];
                $friendshipStatus['requester_id'] = $row['requester_id'];
            }
            else{
                $friendshipStatus['status'] = 'none';
            }
        }

        return $friendshipStatus;
    }

        
    /**
     * requestFriendship Genera solicitud de amistad
     *
     * @param  mixed $friend_id
     * @return void
     */
    public function requestFriendship($friend_id)
    {
        $me = $this->getId();
        $status = 'none';
        $query = "INSERT INTO amistades VALUES (:me, :friend_id, 'pending')";

        $stmt = DBConnection::getSTMT($query);

        $params = [
            'me' => $me,
            'friend_id' => $friend_id
        ];

        $exito = $stmt->execute($params);
        $status='failed';
        if ($exito) {
            $status = 'ok';
        }
        return $status;
    
    }

    
    /**
     * modifyFriendship Modifica estado de amistad
     *
     * @param  mixed $modification tipo de modificación
     * @param  mixed $user id del amigo a modificar
     * @return void
     */
    public function modifyFriendship($modification, $user){
        $me = $this->getId();
        $status = 'none';
        if($modification == 'reject'){
            $query = "DELETE FROM amistades WHERE (requester_id= :user AND requested_id= :me) OR (requested_id= :user AND requester_id= :me)";
        }
        else{
            $query = "UPDATE amistades SET estado= :modification WHERE requester_id= :user AND requested_id= :me";
        }
        

        $stmt = DBConnection::getSTMT($query);

        $params = [
            'me' => $me,
            'user' => $user,
            'modification' => $modification
        ];

        $exito = $stmt->execute($params);

        if ($exito) {
            $status = 'success';
        }
        return $status;
    }

        
    /**
     * getFriendRequests Novedades de amistad
     *
     * @return number
     */
    public function getFriendRequests(){
        $me = $this->getId();
        $query = "SELECT count(*) as requestcount FROM amistades WHERE requested_id = :me AND estado='pending'";
        $stmt = DBConnection::getSTMT($query);

        $params = [
            'me' => $me
        ];
        $exito = $stmt->execute($params);

        if ($exito) {
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $status = $row;
            }
        }
        return $status;
    }

        
    /**
     * getFriends Devuelve perfiles divididos por amigos/solicitados/solicitudes/sugerencias
     *
     * @return void
     */
    public function getFriends(){
        $me = $this->getId();
        $query = "SELECT u.id, u.username, u.foto, a.estado, a.requester_id, a.requested_id FROM users u, amistades a WHERE (u.id = a.requester_id OR u.id = a.requested_id) AND (a.requester_id = $me OR a.requested_id = $me) AND NOT u.id = $me UNION SELECT id, username, foto, 'suggestion' as estado, 0 as requester_id, 0 as requested_id FROM users WHERE id NOT IN (SELECT requester_id FROM amistades WHERE requested_id = $me UNION ALL SELECT requested_id FROM amistades WHERE requester_id = $me) AND NOT ID = $me";
        $stmt = DBConnection::getSTMT($query);
  
        $params = [
            'me' => $me
        ];
        $stmt->execute($params);
        $result = [ "friends" => [], "requestedByMe" => [], "toAccept" => [], "suggestions" => []];
        

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $item = [
                "id" => $row["id"],
                "username" => $row["username"],
                "foto" => $row["foto"]
            ];
            if($row['estado'] == 'accepted'){
                array_push($result['friends'], $item);
            }
            else if($row['estado'] == 'suggestion'){
                array_push($result['suggestions'], $item);
            }
            else{
                if($row['requester_id'] == $me){
                    array_push($result['requestedByMe'], $item);
                }
                else{
                    array_push($result['toAccept'], $item);
                }
            }
        }
		return $result;
    }

        
    /**
     * getProfileFriends Busca los amigos de un perfil (version light de la anterior)
     *
     * @return void
     */
    public function getProfileFriends(){
        $profileid = $this->getId();
        $query = "SELECT id, username, foto FROM users WHERE id IN (SELECT requester_id FROM amistades WHERE requested_id = $profileid AND estado = 'accepted' UNION ALL SELECT requested_id FROM amistades WHERE requester_id = $profileid AND estado = 'accepted')";
        $stmt = DBConnection::getSTMT($query);

        $stmt->execute();
        $friends = [];
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $friends[] = $row;
        }
        return $friends;
    }
    
    /**
     * hasFavNews Busca si un usuario tiene novedades en sus favs
     *
     * @return number
     */
    public function hasFavNews(){
        $me = $this->getId();
        $query = "SELECT count(*) as newscount FROM favoritos WHERE user_id = :me AND has_modifications= true";
        $stmt = DBConnection::getSTMT($query);

        $params = [
            'me' => $me
        ];
        $exito = $stmt->execute($params);

        if ($exito) {
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $status = $row;
            }
        }
        return $status;
    }


    /**
     * Get the value of username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of username
     *
     * @return  self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of foto
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set the value of foto
     *
     * @return  self
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get the value of descripcion
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get the value of residencia
     */
    public function getResidencia()
    {
        return $this->residencia;
    }

    /**
     * Set the value of residencia
     *
     * @return  self
     */
    public function setResidencia($residencia)
    {
        $this->residencia = $residencia;

        return $this;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of token
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set the value of token
     *
     * @return  self
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }
}
