<?php

namespace Social\Core;

class Route
{
    /**
     * Array donde se guardan las rutas
     *
     * @var array
     */
    protected static $routes = [
        'GET' => [],
        'POST' => [],
        'PUT' => [],
        'DELETE' => [],
    ];

    /**
     * Función a ejecutar del controller seleccionado
     *
     * @var String
     */
    protected static $functionToExecute;

    /**
     * Parámetros que vienen en la url, de existir
     *
     * @var array
     */
    protected static $params = [];

    /**
     * Crea una ruta a partir de la url
     *
     * @param String $method
     * @param String $url
     * @param String $controller
     * @return void
     */
    public static function create($method, $url, $controller)
    {
        $method = strtoupper($method);
        self::$routes[$method][$url] = $controller;
    }

    /**
     * Define si la ruta especificada existe
     *
     * @param String $method
     * @param String $url
     * @return void
     */
    public static function exists($method, $url)
    {
        $exists =  self::nonParametrizedRouteExists($method,$url)|| self::parameterizedRouteExists($method, $url);

        if ($exists) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verifica la existencia de la ruta sin parámetros en el array estático $ROUTES
     *
     * @param String $method
     * @param String $url
     * @return void
     */
    public static function nonParametrizedRouteExists($method, $url){
        return isset(self::$routes[$method][$url]);
    }

    /**
     * Verifica la existencia de la ruta con parámetros en el array estático $ROUTES
     *
     * @param String $method
     * @param String $url
     * @return void
     */
    public static function parameterizedRouteExists($method, $url)
	{
        $exists = false;
		$routes = self::$routes[$method];

		$urlParts = explode('/', $url);

		foreach($routes as $route => $functionToExecute) {
			$routeParts = explode('/', $route);

			if(sizeof($urlParts) == sizeof($routeParts)) {
				if(self::compare($urlParts, $routeParts)) {
					self::$functionToExecute = $functionToExecute;
					$exists = true;
				}
			}
		}
		return $exists;
    }
    
    /**
     * Cuenta parámetros para chequear que las partes matcheen con la ruta registrada
     *
     * @param array $urlParts
     * @param array $routeParts
     * @return boolean
     */
    public static function compare($urlParts, $routeParts)
	{
        $params = [];
        $matches = true;
        
		foreach($routeParts as $key => $value) {
			if($value != $urlParts[$key]) {
				if(strpos($value, '{') === 0) {
					$paramName = substr($value, 1, -1);
                    $params[$paramName] = $urlParts[$key];
                   
				} else {
					$matches = false;
				}
			}
		}
        self::setParams($params);
		return $matches;
	}


    /**
     * Devuelve funcion a ejecutar dentro del controlador tomándola del array routes o de la variable asignada en caso de ruta con parámetros
     *
     * @param string $method
     * @param string $url
     * @return string
     */
    public static function getController($method, $url)
    {
        $controller = null;
        if (!is_null(self::$functionToExecute)) {
            $controller = self::$functionToExecute;
        }
        else{
            $controller = self::$routes[$method][$url];
        }

        return $controller;
    }

    /**
     * Función estática de tipo get que devuelve los parámetros también estáticos
     *
     * @return array
     */
    public static function getParams()
    {
        return self::$params;
    }

    /**
     * Setter de parámetros
     *
     * @param array $obj
     * @return void
     */
    public static function setParams($obj)
    {
        self::$params = $obj;
    }
}
