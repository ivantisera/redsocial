<?php
namespace Social\Validation;

class Validator
{
    protected $data = [];
    protected $rules = [];
    protected $errors = [];

    /**
     * Constructor
     *
     * @param Array $data
     * @param Array $rules
     */
    public function __construct($data, $rules)
    {
        $this->data = $data;
        $this->rules = $rules;
        $this->validate();
    }

    /**
     * Realiza las validacione
     *
     * @return void
     */
    public function validate()
    {
        foreach ($this->rules as $fieldName => $fieldRules) {
            foreach ($fieldRules as $ruleName) {
                $this->executeRule($fieldName, $ruleName);
            }
        }
    }

    /**
     * Ejecuta una validacion
     *
     * @param String $fieldName
     * @param String $ruleName
     * @return void
     */
    public function executeRule($fieldName, $ruleName)
    {
        if (strpos($ruleName, ':') === false) {
            $methodName = "_" . $ruleName;
            if (!method_exists($this, $methodName)) {
                throw new Exception("La regla $ruleName no existe.");
            }
            $this->{$methodName}($fieldName);
        } else {
            $ruleData = explode(':', $ruleName);

            $methodName = "_" . $ruleData[0];

            if (!method_exists($this, $methodName)) {
                throw new Exception("La regla $ruleName no existe.");
            }
            $this->{$methodName}($fieldName, $ruleData[1]);
        }
    }

    /**
     * Devuelve el resultado de la validación
     *
     * @return void
     */
    public function passes()
    {
        if (count($this->errors) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Devuelve los errores encontrados
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Agrega un error al array
     *
     * @param string $fieldName
     * @param string $msg
     * @return void
     */
    public function addError($fieldName, $msg)
    {

        if (!isset($this->errors[$fieldName])) {
            $this->errors[$fieldName] = [];
        }

        $this->errors[$fieldName][] = $msg;
    }

    /**
     * Verifica que el campo tenga datos
     *
     * @param string $fieldName
     */
    protected function _required($fieldName)
    {
		$value = $this->data[$fieldName];
         
        if (empty(trim($value))) {
            $this->addError($fieldName, "El campo $fieldName no debe estar vacío.");
        }
    }

    /**
     * Verifica que el campo sea numérico.
     *
     * @param string $fieldName
     */
    public function _numeric($fieldName)
    {
        $value = $this->data[$fieldName];
        if (!is_numeric($value)) {
            $this->addError($fieldName, "El campo $fieldName debe tener un valor numérico.");
        }
    }

    /**
     * Verifica que tenga al menos $minLength caracteres.
     *
     * @param string $fieldName
     * @param int $minLength
     */
    public function _min($fieldName, $minLength)
    {
        $value = $this->data[$fieldName];
        if (strlen($value) < $minLength) {
            $this->addError($fieldName, "El campo $fieldName debe tener al menos  $minLength caracteres.");
        }
    }

    /**
     * Verifica que tenga como maximo $maxLength caracteres.
     *
     * @param string $fieldName
     * @param int $maxLength
     */
    public function _max($fieldName, $maxLength)
    {
        $value = $this->data[$fieldName];
        if (strlen($value) > $maxLength) {
            $this->addError($fieldName, "El campo $fieldName debe tener como máximo $maxLength");
        }
    }

    /**
     * Verifica que el campo mail tenga formato correcto
     *
     * @param string $fieldName
     */
    public function _email($fieldName)
    {
        $value = $this->data[$fieldName];
        $mail_regexp = "/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix";

        if (!preg_match($mail_regexp, $value)) {
            $this->addError($fieldName, "El campo $fieldName no es un correo electrónico válido");
        }
    }

      /**
     * Verifica campos alfanumericos
     *
     * @param string $fieldName
     */
    public function _alphanumeric($fieldName)
    {
        $value = $this->data[$fieldName];
        $alpha_regexp = "/^([A-Za-z0-9\s\.\/\-\?\:\(\)\,\'\+])*$/";

        if (!preg_match($alpha_regexp, $value)) {
            $this->addError($fieldName, "No se permiten caracteres especiales en $fieldName");
        }
    }
}
