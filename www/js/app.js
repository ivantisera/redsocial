// Ionic social App

angular.module('social', ['ionic', 'social.controllers', 'social.services', 'social.directives'])

  .run(function ($ionicPlatform, $rootScope, Auth, $ionicPopup, $state) {

    $ionicPlatform.ready(function () {

      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {

        StatusBar.styleDefault();
      }

      $rootScope.$on('$stateChangeStart', function (ev, nuevoState, nuevoStateParams, actualState) {
        if (nuevoState.data !== undefined && nuevoState.data.requireAuth === true) {
          if (!Auth.isLogged()) {
            ev.preventDefault();

            $ionicPopup.alert({
              'title': '¡No estás logueado!',
              'template': 'Ingresá con tu cuenta y reintenta'
            }).then(function () {
              $state.go('login');
            });
          }
        }
      });
    });
  })




  .constant('API', 'http://localhost/social/api/public/index.php')

  .constant('API_IMG', 'http://localhost/social/api/public/')

  .config(function ($ionicConfigProvider) {
    $ionicConfigProvider.views.maxCache(0);
  });

angular.module('social.controllers', []);
angular.module('social.services', []);
angular.module('social.directives', []);