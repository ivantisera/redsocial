<?php

spl_autoload_register(function ($clase) {
    $clase = str_replace('\\', '/', $clase);
    $archivo = '../app/' . $clase . ".php";

    if (file_exists($archivo)) {
        include $archivo;
    }
});
