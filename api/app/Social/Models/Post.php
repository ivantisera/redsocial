<?php

namespace Social\Models;

use Social\DB\DBConnection;
use Social\Storage\Session;
use PDO;
use JsonSerializable;

class Post extends Model implements JsonSerializable
{
	protected $table 		= "posts";
	protected $primaryKey 	= "id";
	protected $attributes 	= ['id', 'autor_id', 'tipo_id', 'contenido', 'fecha'];

	protected $id;
	protected $autor_id;
	protected $tipo_id;
	protected $contenido;
	protected $fecha;

	public function JsonSerialize()
	{
		return [
			'id' 	        => $this->id,
			'autor_id' 		=> $this->autor_id,
			'tipo_id' 		=> $this->tipo_id,
			'contenido' 	=> $this->contenido,
			'fecha' 		=> $this->fecha,
		];
	}

	/**
	 * Devuelve el POST con todos sus datos 
	 *
	 * @param string $id
	 * @return $POST
	 */
	public static function getPostFromId($id)
	{
		$query = "SELECT * FROM posts WHERE id = ?";

		$stmt = DBConnection::getSTMT($query);
		
		if ($stmt->execute([$id])) {
			if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$post = new static;
				$post->loadDataFromRow($row);
				return $post;
			}
		} else {
			return null;
		}
	}

	
	/**
	 * getFullPosts Todos los posts (de amigos)
	 *
	 * @param  number $me 
	 * @return void
	 */
	public static function getFullPosts($me)
	{	
		$query = "select p.id, p.autor_id, u.username, p.tipo_id, p.contenido, p.fecha from posts p join users u on u.id = p.autor_id where p.autor_id in (select requester_id from amistades where requested_id = $me and estado = 'accepted' union select requested_id from amistades where requester_id = $me and estado = 'accepted' ) or p.autor_id = $me ORDER BY fecha DESC";

		$stmt = DBConnection::getSTMT($query);

		$stmt->execute();

		$result = [];
		$favs = self::getFavourites($me);
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$row['isFav'] = sizeof($favs) > 0 && in_array($row['id'], $favs);
				$result[] = $row;
		}
		return $result;
	}

		
	/**
	 * getFavPosts devuelve posteos que se marcaron como favs
	 *
	 * @param  mixed $me
	 * @return void
	 */
	public static function getFavPosts($me){
		$query = "select p.id, p.autor_id, u.username, p.tipo_id, p.contenido, p.fecha, f.has_modifications from posts p join users u on u.id = p.autor_id join favoritos f on p.id = f.post_id where (p.autor_id in (select requester_id from amistades where requested_id = $me and estado = 'accepted' union select requested_id from amistades where requester_id = $me and estado = 'accepted' ) or p.autor_id = $me) ORDER BY has_modifications DESC, fecha DESC";

		$stmt = DBConnection::getSTMT($query);

		$stmt->execute();
		$result = [];
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$result[] = $row;
		}
		return $result;
	}
	
	/**
	 * getUsersPosts Devuelve los posts de un usuario
	 *
	 * @param  int $id
	 * @param  int $ownerId
	 * @return void
	 */
	public static function getUsersPosts($id, $ownerId)
	{
		$favs = self::getFavourites($ownerId);
		$query = "select p.id, p.autor_id, u.username, p.tipo_id, p.contenido, p.fecha from posts p join users u on u.id = p.autor_id where p.autor_id = $id ORDER BY FECHA DESC";

		$stmt = DBConnection::getSTMT($query);

		$stmt->execute();

		$result = [];
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$row['isFav'] = sizeof($favs) > 0 && in_array($row['id'], $favs);
			$result[] = $row;
		}
		return $result;
	}

		
	/**
	 * getFavourites Devuelve lista ids de posts favs
	 *
	 * @param int $id usuario
	 * @return array 
	 */
	public static function getFavourites($id)
	{
		$query = "select post_id from favoritos where user_id = $id";
		$stmt = DBConnection::getSTMT($query);
		$stmt->execute();
		$result = [];
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$result[] = $row['post_id'];
		}
		return $result;
	}

		
	/**
	 * markAsFavouriteOf Marca post como favorito del usuario logueado
	 *
	 * @param  int $id_user
	 * @return string $status
	 */
	public function markAsFavouriteOf($id_user)
	{
		$status = 'none';
		$this_id = $this->getId();
        $query = "INSERT INTO FAVORITOS VALUES ($this_id, $id_user, 'false')";

        $stmt = DBConnection::getSTMT($query);

        $exito = $stmt->execute();
        $status='failed';
        if ($exito) {
            $status = 'ok';
        }
        return $status;
	}
	
	/**
	 * deleteFavouriteOf Eliminar post de favoritos del usuario pasado 
	 *
	 * @param  int $id_user
	 * @return string $status
	 */
	public function deleteFavouriteOf($id_user){
		$status = 'none';
		$this_id = $this->getId();
		$query = "DELETE FROM FAVORITOS WHERE post_id = $this_id AND user_id = $id_user";
		$stmt = DBConnection::getSTMT($query);

        $exito = $stmt->execute();
        $status='failed';
        if ($exito) {
            $status = 'ok';
        }
        return $status;
	}
		
	/**
	 * modifyFavMark Modifica marca de fav
	 *
	 * @param  mixed $mark
	 * @return boolean
	 */
	public function modifyFavMark($mark){
		$this_id = $this->getId();
		$query = "UPDATE FAVORITOS SET has_modifications = $mark WHERE post_id = $this_id";
		$stmt = DBConnection::getSTMT($query);
		return $stmt->execute();
	}
	
	/**
	 * markAsRead Marca un post favorito como leido
	 *
	 * @param  int $profile_id
	 * @return void
	 */
	public function markAsRead($profile_id){
		$this_id = $this->getId();
		$query = "UPDATE FAVORITOS SET has_modifications = false WHERE post_id = $this_id AND user_id = $profile_id";
		$stmt = DBConnection::getSTMT($query);
		return $stmt->execute();
	}



	/**
	 * Get the value of id
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get the value of autor_id
	 */
	public function getAutor_id()
	{
		return $this->autor_id;
	}

	/**
	 * Set the value of autor_id
	 *
	 * @return  self
	 */
	public function setAutor_id($autor_id)
	{
		$this->autor_id = $autor_id;

		return $this;
	}

	/**
	 * Get the value of tipo_id
	 */
	public function getTipo_id()
	{
		return $this->tipo_id;
	}

	/**
	 * Set the value of tipo_id
	 *
	 * @return  self
	 */
	public function setTipo_id($tipo_id)
	{
		$this->tipo_id = $tipo_id;

		return $this;
	}

	/**
	 * Get the value of contenido
	 */
	public function getContenido()
	{
		return $this->contenido;
	}

	/**
	 * Set the value of contenido
	 *
	 * @return  self
	 */
	public function setContenido($contenido)
	{
		$this->contenido = $contenido;

		return $this;
	}

	/**
	 * Get the value of fecha
	 */
	public function getFecha()
	{
		return $this->fecha;
	}

	/**
	 * Set the value of fecha
	 *
	 * @return  self
	 */
	public function setFecha($fecha)
	{
		$this->fecha = $fecha;

		return $this;
	}
}
