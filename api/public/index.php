<?php
require '../autoload.php';
require '../vendor/autoload.php';



$path = realpath(__DIR__ . "/../");

$path = str_replace('\\', '/', $path);

require $path . "/app/routes.php";

$app = new \Social\Core\App($path);

$app->run();
?>