(function () {

    'use strict';


    angular.module('social.controllers')
        .controller('PostsController', PostsController);

    function PostsController($scope, $state, PostsFactory, API, Storage, $ionicPopup, Auth) {
        var vm = this;
        var operations = {
            posts: [],
            hasError: false,
            errorMsg: null,
        };

        vm.operations = operations;
        vm.goToProfile = goToProfile;
        vm.goToMyProfile = goToMyProfile;
        vm.exit = exit;

        init();

        function init() {
            if (!Auth.isLogged()) {
                $state.go("login");
            }
            preparePosts();

        }
        /**
         * Trae los posts de la api
         * @return {void}
         */
        function preparePosts() {
            PostsFactory.getAllPosts().then(function (response) {
                if (response.status == "error") {
                    vm.operations.hasError = true;
                    vm.operations.errorMsg = response.data;
                }
                else {
                    vm.operations.hasError = false;
                    vm.operations.posts = PostsFactory.formatObject(response.data.data);
                }

            }).catch(function (error) {
                operations.hasError = true;
                operations.errorMsg = "Esta operación no puede ser realizada en este momento. Por favor reintente"
            });

        }


        /**
         * Redirección a Perfil
         *
         * @param int idProfile
         */
        function goToProfile(idProfile) {
            $state.go("profile", { "id": idProfile });
        };

        /**
         * Redireccion al perfil del usuario
         */
        function goToMyProfile() {
            var id = Storage.get("userInfo").id;
            $state.go("profile", { "id": id });
        }

        /**
         * Funcion que ejecuta el logout
         */
        function exit() {
            Auth.logout;
            $state.go("login");
        }


    }
})();



