(function () {

    'use strict';

    angular.module('social.controllers')
        .controller('ProfileController', ProfileController);

    function ProfileController($scope, $q, ProfileFactory, PostsFactory, $stateParams, $state, $ionicPopup, $ionicScrollDelegate, Storage, Auth) {
        var vm = this;
        var operations = {
            profile: null,
            me: {},
            loading: false,
            hasErrors: false,
            errors: [],
            errorTitle: "",
            mine: false,
            hasFriendRequests: false,
            hasFavNews: false,
            buttons: [],
            commentContent: ""
        };

        vm.operations = operations;
        vm.sendPost = sendPost;
        vm.isThisMyProfile = isThisMyProfile;


        vm.exit = exit;
        vm.goSeeFriends = goSeeFriends;
        vm.requestFriendship = requestFriendship;
        vm.acceptFriendship = acceptFriendship;
        vm.rejectFriendship = rejectFriendship;
        vm.goSeeFavs = goSeeFavs,

        init();

        /**
         * Se ejecuta al cargar
         */
        function init() {
            operations.profile = null;
            getThis();
            operations.hasErrors = false;
            operations.errorTitle = "";
            operations.errors = [];
            getMe();

        }

        /**
         * Arma la botonera inferior de manera dinámica
         */
        function createBtns() {
            operations.buttons = [
                { show: () => { return operations.profile.friendship == 'none' && !operations.loading }, clickAction: () => { requestFriendship() }, class: "icon-user-icon", label: "Agregar a amigos" },
                { show: () => { return operations.mine }, clickAction: () => { goToEdit(operations.me.id) }, class: "icon-edit-btn-icon", label: "Editar Perfil" },
                { show: () => { return operations.mine }, clickAction: () => { goSeeFavs() }, class: "icon-star-full-icon", label: "Mis Favoritos" },
                { show: () => { return true; }, clickAction: () => { goSeeFriends() }, class: "icon-users-icon", label: "Ver Amigos" }
            ]
        }

        /**
         * Setea los datos propios
         */
        function getMe() {
            operations.me = Storage.get('userInfo');
        }

        /**
         * Va a editar perfil
         */
        function goToEdit() {
            $state.go("editprofile", { profile: operations.profile });
        }

        /**
         * Va a lista de amigos
         */

        function goSeeFriends() {
            var idProfile = operations.profile.id;
            $state.go("seefriends", { "id": idProfile });
        }


        /**
         * Funcion que ejecuta el logout
         */
        function exit() {
            Auth.logout;
            $state.go("login");
        }

        /**
         * Redirige a Noticias
         */
        function goPosts() {
            $state.go('posts');
        }

        /**
         * Permite mostrar u ocultar la opcion de postear si el perfil es propio
         * @return {boolean}
         */
        function isThisMyProfile() {
            operations.mine = operations.me.id == operations.profile.id;
        }

        /**
         * Llama a la api y carga las variables del perfil pedido
         * @return {void}
         * 
         */
        function getThis() {

            operations.loading = true;
            ProfileFactory.getProfile($state.params.id).then(function (response) {
                var data = response.data.data;
                operations.profile = data.profile;
                operations.profile.friendship = data.friendship;
                operations.profile.requestedByMe = data.requestedByMe;
                operations.posts = PostsFactory.formatObject(data.posts);

                if (angular.isUndefinedOrNullOrEmpty(operations.profile.foto)) {
                    operations.profile.foto = "img/avatar.jpg";
                }
                operations.hasError = false;
            }).catch(function (error) {
                operations.hasError = true;
                operations.errorMsg = "Error. Por favor reintente"
            }).finally(function () {
                isThisMyProfile();
                operations.loading = false;
                createBtns();
                if (operations.mine) {
                    getNews();
                }
            });
        }

        /**
         * Llama a la api para chequear novedades. Activa notificaciones de solicitudes de amistad y cambios en los favoritos.
         * Se llama al cargar el perfil si pertece al usuario logueado
         */
        function getNews() {
            operations.loading = true;
            $q.all([ProfileFactory.getFriendsRequests(), ProfileFactory.hasFavNews()]).then(function (responses) {
                operations.hasFriendRequests = responses[0].data.data.requestcount > 0;
                operations.hasFavNews = responses[1].data.data.newscount > 0;
            }).catch(function (error) {
                operations.hasError = true;
                operations.errorMsg = "Error. Por favor reintente"
            }).finally(() => {
                operations.loading = false;
            });
        }

        /**
         * Se ejecuta al solicitar amistad en perfiles no propios.
         * Llama a la API y actualiza el estado en la vista (Solicitud enviada)
         */
        function requestFriendship() {
            var params = { "requested": operations.profile.id };
            operations.loading = true;
            ProfileFactory.requestFriendship(params).then(function (response) {
                if (response.data.data == 'ok') {
                    operations.profile.friendship = 'pending';
                    operations.profile.requestedByMe = true;
                }
            }).catch(function (error) {
                post.hasError = true;
                post.errorMsg = "Esta operación no puede ser realizada en este momento. Por favor reintentá o esperá unos minutos"
            }).finally(function () {
                operations.loading = false;
            });
        }

        /**
         * Se ejecuta al aceptar una solicitud de amistad (solo se muestra en perfiles ajenos que solicitaron amistad al user loguedo)
         */
        function acceptFriendship() {
            var params = { "id": vm.operations.profile.id };
            operations.loading = true;
            ProfileFactory.acceptFriendship(params).then(function (response) {
                if (response.data.data == 'success') {
                    operations.profile.friendship = 'accepted';
                    getThis();
                }
            }).catch(function (error) {
                post.hasError = true;
                post.errorMsg = "Esta operación no puede ser realizada en este momento. Por favor reintentá o esperá unos minutos"
            }).finally(function () {
                operations.loading = false;
            });
        }

        /**
         * Se ejecuta al rechazar un pedido de amistad o cancelar uno propio.
         */
        function rejectFriendship() {
            var params = { "requester": vm.operations.profile.id };
            operations.loading = true;
            ProfileFactory.rejectFriendship(params).then(function (response) {
                if (response.data.data == 'success') {
                    operations.profile.friendship = 'none';
                    getThis();
                }
            }).catch(function (error) {
                post.hasError = true;
                post.errorMsg = "Esta operación no puede ser realizada en este momento. Por favor reintentá o esperá unos minutos"
            }).finally(function () {
                operations.loading = false;
            });
        }



        /**
         * Se ejecuta al presionar "enviar" el posteo. LLama a la api y carga errores de validacion de haberlos
         * @param {string} comm 
         */
        function sendPost() {
            var autor_id = vm.operations.profile.id

            if (!angular.isUndefinedOrNullOrEmpty(operations.commentContent)) {
                var obj = {
                    contenido: operations.commentContent,
                    autor: autor_id
                };
            } else {
                operations.hasErrors = true;
                operations.errorTitle = "No podés hacer posts sin contenido!";
                return;
            }

            ProfileFactory.sendPost(obj).then(function (resp) {
                var response = resp.data;

                if (response.status == "ok") {
                    vm.operations.hasErrors = false;
                    vm.operations.errorTitle = "";
                    vm.operations.errors = [];
                    $ionicPopup.alert({
                        'title': 'Perfecto',
                        'template': 'Posteo registrado'
                    });

                    operations.posts.unshift(PostsFactory.formatNewPost(response.data.post));
                    operations.commentContent = "";
                } else if (response.status == "error") {

                    vm.operations.hasErrors = true;
                    vm.operations.errorTitle = response.data.info;
                    vm.operations.errors = [];

                    angular.forEach(response.data.validation, function (item) {
                        angular.forEach(item, function (errorText) {
                            vm.operations.errors.push(errorText);
                        })
                    });

                }
            }).catch(function () {
                operations.hasErrors = true;
                operations.errorTitle = "Error de carga. Por favor reintente";
            });
        }

        /**
         * Navega a página de post favoritos
         */
        function goSeeFavs() {
            var idProfile = operations.profile.id;
            $state.go("myfavourites", { "id": idProfile });
        }
    }

})();



