(function () {

    'use strict';


    angular.module('social.controllers')
        .controller('LoginController', LoginController);

    function LoginController($scope, $state, $ionicPopup, Auth, Storage, User) {
        var vm = this;
        var operations = {
            
        };

        vm.operations = operations;
        vm.login = login;
        vm.logout = logout;

        init();

        function init(){
            logout();
        }

        /**
         * Desloguea al usuario
         * @return {void}
         */
        function logout(){
            User.logout();
        } 
        /**
         * Loguea al usuario
         * @param {obj} user 
         */
        function login(user){
            Auth.login(user).then(function(response){
               
                if(response.status == 'ok'){
                    $state.go('posts');
                }else{
                    $ionicPopup.alert({
                        'title': 'Correo o clave incorrectas',
                        'template': "Error al intentar loguearse. Por favor intentá nuevamente"
                    });
                }
            });
        }
    }
})();



